<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('courses',function(Blueprint $table)
        {
            $table->json('fileLinks')->nullable();
            $table->json("blocks")->nullable();
            $table->text("shareCode")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('courses',function(Blueprint $table)
        {
            $table->dropColumn(['fileLinks','blocks','shareCode']);
        });
    }
};
