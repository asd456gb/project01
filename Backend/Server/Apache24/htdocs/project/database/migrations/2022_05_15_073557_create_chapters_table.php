<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chapters', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('type');
            $table->integer('order');
            $table->text('desc');
            $table->json('implements')->nullable();
            $table->timestamps();
        });
        Schema::table('chapters',function(Blueprint $table){
            $table->uuid('course_id')->nullable();

            $table->foreign('course_id')
                ->references('id')->on('courses')
                ->cascadeOnUpdate()->nullOnDelete();

            $table->uuid('parent_id')->nullable();

            $table->foreign('parent_id')
                ->references('id')->on('chapters')
                ->cascadeOnUpdate()->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chapters');
    }
};
