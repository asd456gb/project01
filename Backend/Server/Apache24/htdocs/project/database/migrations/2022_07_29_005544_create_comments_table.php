<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('content');
            $table->integer('star')->nullable();
            $table->integer('good_num');
            $table->integer('bad_num');
            $table->timestamps();

            // foreign course_id parent_id user_account
            $table->uuid('course_id')->nullable();

            $table->foreign('course_id')
                ->references('id')->on('courses')
                ->cascadeOnUpdate()->cascadeOnDelete();

            $table->string('user_account')->nullable();

            $table->foreign('user_account')
                ->references('account')->on('users')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
        Schema::table('comments',function (Blueprint $table) {
            $table->uuid('parent_id')->nullable();

            $table->foreign('parent_id')
                ->references('id')->on('comments')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
};
