<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reactions', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->boolean('status');
            $table->timestamps();

            $table->uuid('comment_id')->nullable();

            $table->foreign('comment_id')
                ->references('id')->on('comments')
                ->cascadeOnUpdate()->cascadeOnDelete();

            $table->string('user_account')->nullable();

            $table->foreign('user_account')
                ->references('account')->on('users')
                ->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reactions');
    }
};
