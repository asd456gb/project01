<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->string('type');
            $table->integer('max_age');
            $table->integer('min_age');
            $table->text('intro')->nullable();
            $table->text('desc')->nullable();
            $table->timestamps();
        });
        Schema::table('courses',function(Blueprint $table){
            $table->string('user_account')->nullable();

            $table->foreign('user_account')
                ->references('account')->on('users')
                ->cascadeOnUpdate()->nullOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
};
