<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('materials', function (Blueprint $table) {
            $table->string('name');
            $table->string('path');
            $table->timestamps();
            $table->string('user_account')->nullable();
            $table->foreign('user_account')
                ->references('account')->on('users')
                ->cascadeOnUpdate()->cascadeOnDelete();
            $table->primary(['name','user_account']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('materials');
    }
};
