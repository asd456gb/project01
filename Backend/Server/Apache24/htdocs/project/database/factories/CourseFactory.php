<?php

namespace Database\Factories;

use App\Models\Course;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class CourseFactory extends Factory
{
    protected $model=Course::class;

    private static $type=['HoC','basic','advanced'];
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'id' => Str::uuid(),
            'name' => $this->faker->sentence(),
            'type' => self::$type[$this->faker->randomDigit()%3],
            'min_age' => rand(6,15),
            'max_age' => rand(20,25),
            'intro' => $this->faker->text(),
            'user_account' => 'u0833011',
        ];
    }
}
