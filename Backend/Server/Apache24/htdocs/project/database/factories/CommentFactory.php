<?php

namespace Database\Factories;

use App\Models\Comment;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Comment>
 */
class CommentFactory extends Factory
{
    protected $model=Comment::class;
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'id' => Str::uuid(),
            'content' => $this->faker->sentence(),
            'star' => rand(1,10),
            'course_id' => '32a79cad-0a57-4630-b15e-6f09f5df4d58'
        ];
    }
}
