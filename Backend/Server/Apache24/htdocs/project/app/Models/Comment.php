<?php

namespace App\Models;

use Database\Factories\CommentFactory;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',
        'content',
        'star',
        'good_num',
        'bad_num',

        'course_id',
        'parent_id',
        'user_account',

        'created_at',
        'updated_at'
    ];
    protected $attributes = [
        'good_num' => 0,
        'bad_num' => 0
    ];
    protected $appends = [
        'reaction','reply'
    ];
    protected $hidden =[
        'replies'
    ];

    protected static function newFactory()
    {
        return CommentFactory::new();
    }

    public function course()
    {
        return $this->belongsTo(Course::class);
    }

    public function replies()
    {
        return $this->hasMany(Comment::class,'parent_id');
    }

    protected function reaction(): Attribute
    {
        return new Attribute(
            get:function()
            {
                if(!Reaction::where('comment_id',$this->id)->where('user_account',auth()->user()->account)->count())
                    return null;
                $reaction=Reaction::where('comment_id',$this->id)->where('user_account',auth()->user()->account)->first();
                return $reaction->status;
            }
        );
    }

    protected function reply(): Attribute
    {
        return new Attribute(
            get:function()
            {
                return $this->replies;
            }
        );
    }
}
