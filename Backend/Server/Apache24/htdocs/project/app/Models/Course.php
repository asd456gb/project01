<?php

namespace App\Models;

use Database\Factories\CourseFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',
        'name',
        'type',
        'min_age',
        'max_age',
        'intro',
        'desc',
        'created_at',
        'updated_at',
        'user_account',

        'fileLinks',
        'blocks',
        'shareCode'
    ];

    protected static function newFactory()
    {
        return CourseFactory::new();
    }

    public function chapters()
    {
        return $this->hasMany(Chapter::class);
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
