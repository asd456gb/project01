<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reaction extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',
        'status',

        'comment_id',
        'user_account',

        'created_at',
        'updated_at'
    ];
    protected $casts = [
        'status' => 'boolean'
    ];
}
