<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Help extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',

        'room_id',
        'user_account',
        'problem_desc',
        'status',

        'created_at',
        'updated_at'
    ];
}
