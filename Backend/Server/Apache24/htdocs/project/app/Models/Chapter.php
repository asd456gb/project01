<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chapter extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',
        'name',
        'type',
        'order',
        'desc',

        //option
        'implements',

        'created_at',
        'updated_at',

        'course_id',
        'parent_id'
    ];
}
