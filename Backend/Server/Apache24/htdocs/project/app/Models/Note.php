<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Note extends Model
{
    use HasFactory;

    protected $keyType='string';
    protected $fillable = [
        'id',

        'title',
        'content',

        'user_account',
        'course_id',

        'created_at',
        'updated_at'
    ];
}
