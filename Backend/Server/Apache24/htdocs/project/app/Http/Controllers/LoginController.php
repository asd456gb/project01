<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Cookie;

class LoginController extends Controller
{
    public function __construct()
    {
        //if(request()->cookie('token'))
        $this->middleware('auth:api')->except('login');
    }
    public function login()
    {
        $credentials=request(['account','password']);
        if(!$token=Auth::attempt($credentials))
            return response()->json(['message'=>'invalid credentials'],401);
        $cookie=Cookie::create('token')
            ->withValue($token)->withSecure(true)->withHttpOnly(true)->withSameSite('none');
        return response()->json(['message'=>'success'])->cookie($cookie);
    }
    public function logout()
    {
        Auth::logout();
        return response()->json(['message'=>'Successfully logged out']);
    }
    public function me()
    {
        return response()->json(auth()->user());
    }
}
