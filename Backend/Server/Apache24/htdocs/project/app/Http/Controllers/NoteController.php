<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Note;
use App\Models\Course;

class NoteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        //
        $notes=Note::where('user_account',auth()->user()->account)->where('course_id',$course_id)->get();
        return response($notes);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request=$request->all();
        $request['user_account']=auth()->user()->account;
        $request['id']=Str::uuid();
        $note=Note::create($request);
        return response($note);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($course_id)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        Note::find($id)->update($request->all());
        return response(Note::find($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getNotedCourse()
    {
        //
        $notes=Note::where('user_account',auth()->user()->account)->select('course_id')->distinct()->get();
        $result=array();
        foreach($notes as $note)
            array_push($result,$note->course_id);
        $courses=Course::whereIn('id',$result)->select('id','name')->get();
        return response($courses);
    }
}
