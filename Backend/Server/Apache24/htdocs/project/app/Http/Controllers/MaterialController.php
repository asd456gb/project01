<?php

namespace App\Http\Controllers;

use App\Models\Material;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class MaterialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account)
    {
        //
        $materials=Material::where('user_account',$account)->get();
        return response($materials);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user_account=auth()->user()->account;
        $path=Storage::putFile('project/materials/' . $user_account,$request->material);
        $material=Material::create([
            'name' => $request->name,
            'path' => $path,
            'user_account' => $user_account
        ]);
        return response($material);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($account,$name)
    {
        //
        $material=Material::where('user_account',$account)->where('name',$name)->first();
        $file=Storage::get($material->path);
        $type=Storage::mimeType($material->path);
        return response($file)->header('Content-Type',$type);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
