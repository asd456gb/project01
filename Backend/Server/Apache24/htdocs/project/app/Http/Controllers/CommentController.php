<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Reaction;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($course_id)
    {
        //
        $comments=Comment::where('course_id',$course_id)->where('parent_id',null);
        $pagination=$comments->paginate(5);
        return response($pagination);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request=$request->all();
        $request['user_account']=auth()->user()->account;
        $request['id']=Str::uuid();
        $course=Comment::create($request);
        return response($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function canComment($course_id)
    {
        $comments=Comment::where('course_id',$course_id)->where('user_account',auth()->user()->account)->count();
        return response()->json(['course_id'=>$course_id,'user_account'=>auth()->user()->account,'canComment'=>$comments==0]);
    }

    public function likeOrDislikeComment($id,$operation)
    {
        $comment=Comment::find($id);
        if(!Reaction::where('comment_id',$id)->where('user_account',auth()->user()->account)->count())
        {
            $reaction=array(
                'id'=>Str::uuid(),
                'status'=>$operation=='like'?true:false,
                'comment_id'=>$id,
                'user_account'=>auth()->user()->account
            );
            $reaction=Reaction::create($reaction);
            $comment->increment($operation=='like'?'good_num':'bad_num');
        }
        else
        {
            $reaction=Reaction::where('comment_id',$id)->where('user_account',auth()->user()->account)->first();
            switch($operation)
            {
                case 'like':
                    $comment->increment('good_num');
                    $comment->decrement('bad_num');
                    $reaction->update(array('status'=>true));
                    break;
                case 'dislike':
                    $comment->increment('bad_num');
                    $comment->decrement('good_num');
                    $reaction->update(array('status'=>false));
                    break;
                case 'cancel':
                    if($reaction->status)
                        $comment->decrement('good_num');
                    else
                        $comment->decrement('bad_num');
                    $reaction->delete();
                    break;
            }
        }
        return response(array('operation'=>$operation,'status'=>'success'));
    }
}
