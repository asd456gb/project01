<?php

use App\Http\Controllers\ChapterController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\HelpController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\MaterialController;
use App\Http\Controllers\NoteController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix'=>'auth'],function(){
    Route::post('/login',[LoginController::class,'login']);
    Route::post('/logout',[LoginController::class,'logout']);
    Route::get('/',[LoginController::class,'me']); //for test
});

Route::get('failure',function(){
    return response()->json(['message'=>'Unauthenticated']);
})->name('failure');

Route::middleware('auth:api')->group(function()
{
    Route::apiResource('courses',CourseController::class)->except('destroy');
    Route::get('display/courses',[CourseController::class,'displayCourses']);

    Route::apiResource('chapters',ChapterController::class)->except(['index','show','destroy']);
    Route::get('chapters',[ChapterController::class,'index'])->name('chapters.index');

    Route::apiResource('users',UserController::class);
    Route::post('users/uploadAvatar',[UserController::class,'uploadAvatar'])->name('users.uploadAvatar');
    Route::get('users/avatar/{account}',[UserController::class,'getAvatar']);

    Route::apiResource('materials',MaterialController::class)->except('show','index','update','destroy');
    //Route::get('materials/{account}/{name}',[MaterialController::class,'show'])->name('materials.show');
    Route::get('materials/{account}',[MaterialController::class,'index'])->name('materials.index');

    Route::apiResource('comments',CommentController::class)->except('index','show','update','destroy');
    Route::get('comments/{course_id}',[CommentController::class,'index'])->name('comments.index');
    Route::get('comments/canComment/{course_id}',[CommentController::class,'canComment'])->name('comments.canComment');
    Route::get('comments/reaction/{comment}/{operation}',[CommentController::class,'likeOrDislikeComment'])->name('comments.reaction');
});

// because of p5.js's request without cookie

Route::get('materials/{account}/{name}',[MaterialController::class,'show'])->name('materials.show');

// WIP

Route::apiResource('helps',HelpController::class)->except('destroy');

Route::apiResource('notes',NoteController::class)->except(['index','show','destroy']);
Route::get('notes/{course_id}',[NoteController::class,'index'])->name('notes.index');
Route::get('getNotedCourse',[NoteController::class,'getNotedCourse'])->name('notes.getNotedCourse');