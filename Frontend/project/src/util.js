import MyCourse from "./components/MyCourse";
import MyNote from "./components/MyNote";
import MySetting from "./components/MySetting";
import CourseManage from "./components/CourseManage";
import HelpManage from "./components/HelpManage";

let util={};

util.title="程式設計三學平台";
util.role={
    'Student': '學生',
    'Teacher': '老師',
    'Provider': '教材提供者',
    'Manager': '管理者'
}
util.courseType={
    "HoC": "關卡積木式程式設計",
    "basic": "開放積木式程式設計",
    "advanced": "進階文字式程式設計"
};
util.chapterType={
    "text": "一般多媒體文章",
    "implement": "實作練習"
}
// for sidebar
util.components={
    'MyCourse': <MyCourse/>,
    'MyNote': <MyNote/>,
    'MySetting': <MySetting/>,
    'CourseManage': <CourseManage/>,
    'HelpManage': <HelpManage/>
};

util.helpStatus={
    notProcessed: "未處理",
    processing: "正在處理中",
    processed: "已處理"
}

util.canvasId="displayCanvas";

util.example={
    blockDef:{
        type: "example",
        message0: "測試方塊",
        previousStatement: null,
        nextStatement: null,
        colour: 160,
        tooltip: "",
        helpUrl: ""
    },
    blockCodeDef: "(block)=>{\n\tlet code=\"console.log('Hello World!')\\n\";\n\treturn code;\n}"
}

util.getTitleByPrefix=(prefix)=>{
    return `${prefix} - ${util.title}`;
}

util.frontend={
    convergence: 'desktop-pcchenproxy:8000/api/realtime/convergence/default',
    host: 'desktop-pcchenfrontend'
}

util.backend={
    host: "desktop-pcchenbackend/project/public/api",
};

util.isAdvancedClass=(type)=>{
    return type==="advanced";
}

util.isBasicClass=(type)=>{
    return type==="basic";
}

util.isText=(type)=>{
    return type==='text';
}

util.login=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/auth/login`,{
        method: 'post',
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.logout=async ()=>{
    const json=await fetch(`https://${util.backend.host}/auth/logout`,{
        method: 'post',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getMe=async ()=>{
    const json=await fetch(`https://${util.backend.host}/auth`,
    {
        method: 'get',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.addCourse=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/courses`,
    {
        method: 'post',
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.editCourse=async (data,id)=>{
    const json=await fetch(`https://${util.backend.host}/courses/${id}`,
    {
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data),
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.getCourses=async ()=>{
    const json=await fetch(`https://${util.backend.host}/courses`,
    {
        method: 'get',
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getCourse=async (id)=>{
    const json=await fetch(`https://${util.backend.host}/courses/${id}`,
    {
        method: "get",
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.addChapter=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/chapters`,
    {
        'method': "post",
        body: formData,
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.editChapter=async (data,id)=>{
    const json=await fetch(`https://${util.backend.host}/chapters/${id}`,
    {
        'method': "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(data),
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.getChapters=async (course_id)=>{
    const json=await fetch(`https://${util.backend.host}/chapters?course_id=${course_id}`,
    {
        "method": "get",
        credentials: "include"
    }).then(response=>response.json());
    return json;
}

util.uploadAvatar=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/users/uploadAvatar`,
    {
        method: "post",
        body: formData,
        credentials: 'include'
    }).then(response=>response.text());
    return json;
}

util.uploadMaterial=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/materials`,
    {
        method: "post",
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getMaterialList=async (account)=>{
    const json=await fetch(`https://${util.backend.host}/materials/${account}`,
    {
        method: "get",
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.displayCourses=async (parameters)=>{
    let url=new URL(`https:${util.backend.host}/display/courses`)
    url.search=new URLSearchParams(parameters).toString();
    const json=await fetch(url,
    {
        method: 'get',
        credentials: 'include'
    }).then(async (response)=>{
        let result;
        let clone=response.clone();
        try
        {
            result=await response.json();
        }
        catch(e)
        {
            console.log('error');
            result=await clone.text();
        }
        return result;
    });
    return json;
}

util.addComment=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/comments`,
    {
        method: "post",
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.canComment=async (course_id)=>{
    const json=await fetch(`https://${util.backend.host}/comments/canComment/${course_id}`,
    {
        method: "get",
        credentials: 'include'
    }).then(r=>r.json());
    return json;
}

util.getComments=async (course_id,currentPage=1)=>{
    const json=await fetch(`https://${util.backend.host}/comments/${course_id}?page=${currentPage}`,
    {
        method: "get",
        credentials: 'include'
    }).then(r=>r.json());
    return json;
}

util.makeReaction=async (comment_id,operation)=>{
    const json=await fetch(`https://${util.backend.host}/comments/reaction/${comment_id}/${operation}`,
    {
        method: "get",
        credentials: 'include'
    }).then(r=>r.json());
    return json;
}

util.sendHelpRequest=async (formData)=>{
    const json=await fetch(`https://${util.backend.host}/helps`,{
        method: "post",
        body: formData,
        credentials: 'include'
    }).then(r=>r.json());
    return json;
}

util.getHelps=async ()=>{
    const json=await fetch(`https://${util.backend.host}/helps`,{
        method: "get",
        credentials: "include"
    }).then(r=>r.json());
    return json;
}

util.modifyHelp=async (id,status)=>{
    const json=await fetch(`https://${util.backend.host}/helps/${id}`,{
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify({status}),
        credentials: "include"
    }).then(r=>r.json());
    return json;
}

util.addNote=async(formData)=>{
    const json=await fetch(`https://${util.backend.host}/notes`,
    {
        method: "post",
        body: formData,
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.getNotes=async(course_id)=>{
    const json=await fetch(`https://${util.backend.host}/notes/${course_id}`,
    {
        method: "get",
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

util.modifyNote=async(id,note)=>{
    const json=await fetch(`https://${util.backend.host}/notes/${id}`,{
        method: "put",
        headers: {
            "Content-type": "application/json; charset=UTF-8"
        },
        body: JSON.stringify(note),
        credentials: "include"
    }).then(r=>r.json());
    return json;
}

util.getNotedCourses=async()=>{
    const json=await fetch(`https://${util.backend.host}/getNotedCourse`,
    {
        method: "get",
        credentials: 'include'
    }).then(response=>response.json());
    return json;
}

let lastId=0;

util.newId=()=>{
    return lastId++;
}

util.objCopy=(obj)=>{
    return JSON.parse(JSON.stringify(obj));
}

util.resize=()=>{
    setTimeout(()=>{
        window.dispatchEvent(new Event('resize'));
    },1000);
}

util.getDefaultBasicToolbox=()=>{
    return {
        "kind": "categoryToolbox",
        "contents": [
            {
                "kind": "category",
                "name": "邏輯",
                "categorystyle": "logic_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "controls_if"
                    },
                    {
                        "kind": "block",
                        "type": "logic_compare"
                    },
                    {
                        "kind": "block",
                        "type": "logic_operation"
                    },
                    {
                        "kind": "block",
                        "type": "logic_negate"
                    },
                    {
                        "kind": "block",
                        "type": "logic_boolean"
                    },
                    {
                        "kind": "block",
                        "type": "logic_null"
                    },
                    {
                        "kind": "block",
                        "type": "logic_ternary"
                    }
                ]
            },
            {
                "kind": "category",
                "name": "迴圈",
                "categorystyle": "loop_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "controls_repeat_ext"
                    },
                    {
                        "kind": "block",
                        "type": "controls_whileUntil"
                    },
                    {
                        "kind": "block",
                        "type": "controls_for"
                    },
                    {
                        "kind": "block",
                        "type": "controls_forEach"
                    },
                    {
                        "kind": "block",
                        "type": "controls_flow_statements"
                    }
                ]
            },
            {
                "kind": "category",
                "name": "數學",
                "categorystyle": "math_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "math_number"
                    },
                    {
                        "kind": "block",
                        "type": "math_arithmetic"
                    },
                    {
                        "kind": "block",
                        "type": "math_single"
                    },
                    {
                        "kind": "block",
                        "type": "math_trig"
                    },
                    {
                        "kind": "block",
                        "type": "math_constant"
                    },
                    {
                        "kind": "block",
                        "type": "math_number_property"
                    },
                    {
                        "kind": "block",
                        "type": "math_change"
                    },
                    {
                        "kind": "block",
                        "type": "math_round"
                    },
                    {
                        "kind": "block",
                        "type": "math_on_list"
                    },
                    {
                        "kind": "block",
                        "type": "math_modulo"
                    },
                    {
                        "kind": "block",
                        "type": "math_random_int"
                    },
                    {
                        "kind": "block",
                        "type": "math_random_float"
                    }
                ]
            },
            {
                "kind": "category",
                "name": "文字",
                "categorystyle": "text_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "text"
                    },
                    {
                        "kind": "block",
                        "type": "text_join"
                    },
                    {
                        "kind": "block",
                        "type": "text_append"
                    },
                    {
                        "kind": "block",
                        "type": "text_length"
                    },
                    {
                        "kind": "block",
                        "type": "text_isEmpty"
                    },
                    {
                        "kind": "block",
                        "type": "text_indexOf"
                    },
                    {
                        "kind": "block",
                        "type": "text_charAt"
                    },
                    {
                        "kind": "block",
                        "type": "text_getSubstring"
                    },
                    {
                        "kind": "block",
                        "type": "text_changeCase"
                    },
                    {
                        "kind": "block",
                        "type": "text_trim"
                    },
                    {
                        "kind": "block",
                        "type": "text_print"
                    },
                    {
                        "kind": "block",
                        "type": "text_prompt_ext"
                    }
                ]
            },
            {
                "kind": "category",
                "name": "清單",
                "categorystyle": "list_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "lists_create_with"
                    },
                    {
                        "kind": "block",
                        "type": "lists_repeat"
                    },
                    {
                        "kind": "block",
                        "type": "lists_length"
                    },
                    {
                        "kind": "block",
                        "type": "lists_isEmpty"
                    },
                    {
                        "kind": "block",
                        "type": "lists_indexOf"
                    },
                    {
                        "kind": "block",
                        "type": "lists_getIndex"
                    },
                    {
                        "kind": "block",
                        "type": "lists_setIndex"
                    },
                    {
                        "kind": "block",
                        "type": "lists_getSublist"
                    },
                    {
                        "kind": "block",
                        "type": "lists_split"
                    },
                    {
                        "kind": "block",
                        "type": "lists_sort"
                    }
                ]
            },
            {
                "kind": "category",
                "name": "顏色",
                "categorystyle": "colour_category",
                "contents":[
                    {
                        "kind": "block",
                        "type": "colour_picker"
                    },
                    {
                        "kind": "block",
                        "type": "colour_random"
                    },
                    {
                        "kind": "block",
                        "type": "colour_rgb"
                    },
                    {
                        "kind": "block",
                        "type": "colour_blend"
                    }
                ]
            },
            {
                "kind": "sep",
            },
            {
                "kind": "category",
                "name": "變數",
                "categorystyle": "variable_category",
                "custom":"VARIABLE"
            },
            {
                "kind": "category",
                "name": "函數",
                "categorystyle": "procedure_category",
                "custom":"PROCEDURE"
            }
        ]
    };
}

export default util;