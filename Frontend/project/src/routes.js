import LoginPage from './pages/LoginPage';
import WelcomePage from './pages/WelcomePage';
import DashboardPage from './pages/DashboardPage';
import CoursePage from './pages/CoursePage';

let routes=[
    {
        path: '/',
        component: <WelcomePage/>
    },
    {
        path: '/login',
        component: <LoginPage/>
    },
    {
        path: '/dashboard',
        component: <DashboardPage/>
    },
    {
        path: '/course',
        component: <CoursePage/>
    }

]

export default routes;