/* eslint-disable no-eval */

import React from "react";
import { UserContext } from "../context/UserContext";
import { Widget,addResponseMessage } from "react-chat-widget";
import 'react-chat-widget/lib/styles.css';

import { Convergence } from "@convergence/convergence";

import util from '../util';
import Video from "../components/Video";

import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import RawTool from '@editorjs/raw';
import SimpleImage from '@editorjs/simple-image';
import Checklist from '@editorjs/checklist';
import List from '@editorjs/list';
import Embed from '@editorjs/embed';
import Quote from '@editorjs/quote';
import Editor from "@monaco-editor/react";

import MonacoConvergenceAdapter from "../Class/MonacoConvergenceAdapter";
import "@convergencelabs/monaco-collab-ext/css/monaco-collab-ext.css";

import * as Blockly from 'blockly/core';
import Interpreter from 'js-interpreter';
import BlocklyComponent from "../components/BlocklyComponent";
import NoteEditor from "../components/note/NoteEditor";

class CoursePage extends React.Component
{
    static contextType=UserContext;

    constructor()
    {
        super();
        this.state={
            roomId: null,
            course: null,
            chapters: null,
            currentChapter: null,

            chatRoom: null,
            model: null,

            localStream: null,
            peerList: {},
            streamList: {},

            editorjsDesc: null,
            editorjsDescId: `editorjsDesc-${util.newId()}`,

            resultKey: util.newId(),

            editor: null,
            blockly: null,

            users: [],
            room: null
        }
        this.myVideo=React.createRef();
        this.remoteVideo=React.createRef();

        this.scriptLocation=React.createRef();
        this.problemDesc=React.createRef();
    }
    setCurrentChapter=(currentChapter)=>this.setState({currentChapter})
    renderTree=(list=this.state.chapters,depth=1)=>{
        if(!list) return;
        return list.map((chapter)=>{
            let display=chapter.childChapters.length===0?'none':'unset';

            let id=`collapse-${chapter.id}`;
            return <React.Fragment key={chapter.id}>
                <button className='list-group-item list-group-item-action' style={{textIndent: (depth-1)*1.5+"em"}} onClick={()=>{
                        this.setCurrentChapter(chapter);
                    }}>
                        <div className='form-check form-switch'>
                            <input className='form-check-input' type="checkbox" role="switch" data-bs-toggle="collapse" data-bs-target={`#${id}`} aria-expanded="true" style={{display: display}}></input>
                            <label className="form-check-label">{chapter.name}</label>
                        </div>
                </button>
                <div className="collapse show" id={id}>
                    {this.renderTree(chapter.childChapters,depth+1)}
                </div>
            </React.Fragment>
        })
    }
    // chat room
    handleNewUserMessage=(newMessage)=>{
        let chatRoom=this.state.chatRoom;
        chatRoom.send(JSON.stringify(
            {
                service: 'chat',
                sender: this.context.user.account,
                name: this.context.user.name,
                message: newMessage
            })
        );
    }
    handleWebRTC=(packet)=>{
        let chatRoom=this.state.chatRoom;
        let peerList=this.state.peerList;
        if(packet.type!=='join'&&packet.sender===this.context.user.account)
            return;
        switch(packet.type)
        {
            case "join":
                let users=[];
                for(let user of chatRoom.info().members)
                {
                    if(user.user.displayName)
                        users.push(user.user.displayName);
                }
                if(users.length>1)
                {
                    users.forEach((user)=>{
                        let arr=[this.context.user.account,user];
                        let connectionStr=arr.sort().join("<->");
                        if(!peerList[connectionStr]&&user!==this.context.user.account)
                        {
                            let config={
                                iceServers:[
                                    {
                                        url: "stun:stun.l.google.com:19302"
                                    }
                                ]
                            };
                            let peer=new RTCPeerConnection(config);
                            let localStream=this.state.localStream;
                            localStream.getTracks().forEach((track)=>{
                                peer.addTrack(track,localStream);
                            })
                            peer.onicecandidate=(e)=>{
                                if(e.candidate)
                                {
                                    chatRoom.send(JSON.stringify({
                                        service: 'webrtc',
                                        sender: this.context.user.account,
                                        type: "ice_candidate",
                                        candidate: e.candidate,
                                        connectionStr: connectionStr
                                    }))
                                }
                            }
                            peer.ontrack=(e)=>{
                                let streamList=this.state.streamList;
                                streamList[connectionStr]=e.streams[0];
                                this.setState({streamList});
                            }
                            peerList[connectionStr]=peer;
                            this.setState({peerList});
                        }
                    })
                    if(packet.sender===this.context.user.account)
                    {
                        console.log('broadcast offer');
                        for(let connectionStr in peerList)
                        {
                            let option={
                                offerToReceiveAudio: 1,
                                offerToReceiveVideo: 1
                            }
                            peerList[connectionStr].createOffer(option)
                                .then((desc)=>{
                                    peerList[connectionStr].setLocalDescription(desc,()=>{
                                        chatRoom.send(JSON.stringify({
                                            service: 'webrtc',
                                            sender: this.context.user.account,
                                            type: 'offer',
                                            SDP: peerList[connectionStr].localDescription,
                                            connectionStr: connectionStr
                                        }))
                                    })
                                })
                        }
                    }
                }
                break;
            case 'offer':
                if(peerList[packet.connectionStr])
                {
                    peerList[packet.connectionStr].setRemoteDescription(packet.SDP)
                        .then(()=>{
                            peerList[packet.connectionStr].createAnswer()
                                .then((desc)=>{
                                    peerList[packet.connectionStr].setLocalDescription(desc)
                                        .then(()=>{
                                            chatRoom.send(JSON.stringify({
                                                service: 'webrtc',
                                                sender: this.context.user.account,
                                                type: 'answer',
                                                SDP: peerList[packet.connectionStr].localDescription,
                                                connectionStr: packet.connectionStr
                                            }))
                                        })
                                })
                        })
                }
                break;
            case 'ice_candidate':
                if(packet.candidate&&peerList[packet.connectionStr])
                    peerList[packet.connectionStr].addIceCandidate(packet.candidate);
                break;
            case 'answer':
                if(peerList[packet.connectionStr])
                    peerList[packet.connectionStr].setRemoteDescription(packet.SDP);
                break;
            default:
                break;
        }
    }
    waitUserData=(callback)=>{
        if(!this.context.user)
        {
            setTimeout(()=>{
                this.waitUserData(callback);
            },500);
        }
        else
            callback();
    }
    handleLoad=()=>{
        this.setState({resultKey: util.newId()},()=>{
            let div=this.scriptLocation.current;
            div.innerHTML='';
            let script=document.createElement('script');
            script.innerHTML=this.state.course.shareCode;
            div.append(script);
            try
            {
                setTimeout(()=>{
                    let extra_code=JSON.parse(this.state.currentChapter.implements).extra_code;
                    let script=document.createElement('script');
                    script.innerHTML=extra_code;
                    div.append(script);
                },1000);
            }catch(e){}
        })
    }
    initApi=(interpreter,globalObject)=>{
        let blocks=JSON.parse(this.state.course.blocks);
        for(let type in blocks)
        {
            let block=blocks[type];
            let functionName=block.style.functionName;
            if(functionName)
                interpreter.setProperty(globalObject,functionName,interpreter.createNativeFunction(window[functionName]));
        }
        let workspace=this.state.blockly.state.blockly;
        interpreter.setProperty(globalObject,'highlightBlock',interpreter.createNativeFunction((id)=>{
            return workspace.highlightBlock(id);
        }));
    }
    nextStep=(interpreter)=>{
        if(interpreter.step())
        {
            setTimeout(()=>{
                this.nextStep(interpreter);
            },50);
        }
        else
        {
            let result=eval(JSON.parse(this.state.currentChapter.implements).valid_answer_code);
            alert((result)?"過關":"沒有過關");
        }
    }
    handleExecute=()=>{
        if(util.isAdvancedClass(this.state.course.type))
        {
            let result=eval(this.state.editor.getValue());
            result.then(()=>{
                result=eval(JSON.parse(this.state.currentChapter.implements).valid_answer_code);
                alert((result)?"過關":"沒有過關");
            });
        }
        else if(util.isBasicClass(this.state.course.type))
        {
            let code=Blockly.JavaScript.workspaceToCode(this.state.blockly.state.blockly);
            console.log(code);
            let ftn=`async function f(){${code}} f()`;
            let result=eval(ftn);
            result.then(()=>{
                result=eval(JSON.parse(this.state.currentChapter.implements).valid_answer_code);
                alert((result)?"過關":"沒有過關");
            })
        }
        else
        {
            let code=Blockly.JavaScript.workspaceToCode(this.state.blockly.state.blockly);
            let myInterpreter=new Interpreter(code,this.initApi);
            this.nextStep(myInterpreter);
        }
    }
    handleSync=()=>{
        let model=this.state.model;
        let rtObject=model.elementAt('data');
        if(util.isAdvancedClass(this.state.course.type))
        {
            let editor=this.state.editor;
            if(rtObject.value()==="")
                rtObject.value(editor.getValue());
            else
                editor.setValue(rtObject.value());
            let adapter=new MonacoConvergenceAdapter(editor,rtObject);
            adapter.bind();
        }
        else
        {
            let blockly=this.state.blockly.state.blockly;
            let data=Blockly.serialization.workspaces.save(blockly);
            if(rtObject.value()==="")
                rtObject.value(JSON.stringify(data));
            else
                Blockly.serialization.workspaces.load(JSON.parse(rtObject.value()),blockly);
            blockly.addChangeListener((e)=>{
                let data=JSON.stringify(Blockly.serialization.workspaces.save(blockly));
                let previous=this.state.previous;
                if(e instanceof Blockly.Events.UiBase)
                    return;
                else if(e instanceof Blockly.Events.BlockCreate)
                    return;
                else if(e instanceof Blockly.Events.BlockDrag)
                    return;
                else if(data===previous)
                    return;
                this.setState({previous: data});
                rtObject.value(data);
            })
            rtObject.addListener('value',(e)=>{
                if(e.user.display===this.context.user.account)
                    return;
                Blockly.serialization.workspaces.load(JSON.parse(rtObject.value()),blockly);
            })
        }
    }
    handleEditorDidMount=(editor,monaco)=>{
        this.setState({editor});
        editor.setValue(JSON.parse(this.state.currentChapter.implements).example);
    }
    handleBlocklyDidMount=(blockly)=>{
        this.setState({blockly});
        let selected_block=JSON.parse(this.state.currentChapter.implements).selected_block;
        blockly.clearToolbox(()=>{
            blockly.loadBlocksByName(selected_block);
        });
    }
    updateUsers=(members)=>{
        let result=[];
        for(let i=0;i<members.length;i++)
        {
            let user=members[i].user.displayName;
            if(user==="anonymous"||user===this.context.user.account) continue;
            result.push(user);
        }
        this.setState({users:result});
    }
    inviteClick=(e,user)=>{
        let btn=e.target;
        btn.innerText="已邀請";
        btn.classList.remove("btn-primary");
        btn.classList.add("btn-success");
        btn.setAttribute('disabled','');

        this.state.room.send(JSON.stringify({
            from: this.context.user.account,
            to: user,
            type: "invite",
            roomId: this.state.roomId
        }));
    }
    sendHelpRequest=()=>
    {
        let problemTextElement=this.problemDesc.current;
        let formData=new FormData();
        formData.append('problem_desc',problemTextElement.value);
        formData.append('room_id',this.state.roomId);
        util.sendHelpRequest(formData).then(json=>{
            console.log(json);
        })
    }
    getCourse=()=>{
        return this.state.course;
    }
    componentDidMount()
    {
        document.title=util.title;

        let url=new URL(window.location.href);
        let roomId=url.searchParams.get('roomId');
        this.setState({roomId});

        this.waitUserData(()=>{
            Convergence.connectAnonymously(`https://${util.frontend.convergence}`,this.context.user.account)
                .then(async (domain)=>
                {
                    let chatRoom=await domain.chat().join(roomId);
                    let model=await domain.models().open(roomId);
                    chatRoom.on("message",(e)=>{
                        let packet=JSON.parse(e.message);
                        switch(packet.service)
                        {
                            case "chat":
                                if(packet.sender===this.context.user.account)
                                    break;
                                addResponseMessage(`${packet.name}:${packet.message}`);
                                break;
                            case "webrtc":
                                this.handleWebRTC(packet);
                                break;
                            default:
                                break;
                        }
                    })
                    chatRoom.on('user_left',(e)=>{
                        let arr=[this.context.user.account,e.user.displayName];
                        let connectionStr=arr.sort().join("<->");
                        let peerList=this.state.peerList;
                        delete peerList[connectionStr];
                        let streamList=this.state.streamList;
                        delete streamList[connectionStr];
                        this.setState({peerList});
                        this.setState({streamList});
                    })
                    navigator.mediaDevices.getUserMedia({video:true,audio:true})
                        .then(stream=>{
                            this.myVideo.current.srcObject=stream;
                            this.setState({localStream: stream});
                            chatRoom.send(JSON.stringify(
                                {
                                    service: "webrtc",
                                    sender: this.context.user.account,
                                    type: 'join'
                                })
                            );
                        });

                    let course_id=model.elementAt('course_id').value();
                    let course=await util.getCourse(course_id);
                    let chapters=await util.getChapters(course_id);
                    chapters.sort((a,b)=>{
                        return a.order-b.order;
                    })

                    let hashTable={};
                    chapters.forEach((chapter)=>{
                        hashTable[chapter.id]={...chapter,childChapters: []};
                    })
                    let dataTree=[];
                    chapters.forEach((chapter)=>{
                        if(chapter.parent_id)
                            hashTable[chapter.parent_id].childChapters.push(hashTable[chapter.id])
                        else
                            dataTree.push(hashTable[chapter.id]);
                    })

                    this.setState({course});
                    this.setState({chapters: dataTree});
                    this.setState({currentChapter: dataTree[0]},()=>{
                        this.setState({editorjsDesc: new EditorJS({
                            holder: this.state.editorjsDescId,
                            data: JSON.parse(this.state.currentChapter.desc),
                            readOnly: true,
                            tools:{
                                header: Header,
                                raw: RawTool,
                                image: SimpleImage,
                                checkList: {
                                    class: Checklist,
                                    inlineToolbar: true
                                },
                                list: {
                                    class: List,
                                    inlineToolbar: true,
                                    config: {
                                        defaultStyle: 'unordered'
                                    }
                                },
                                embed: Embed,
                                quote: Quote
                            }
                        })})
                    });

                    this.setState({chatRoom});
                    this.setState({model});
                });
            Convergence.connectAnonymously(`https://${util.frontend.convergence}`,'anonymous')
                .then((domain)=>{
                    let chatService=domain.chat();
                    chatService.join('online').then(room=>{
                        this.updateUsers(room.info().members);
                        room.on("USER_JOINED",()=>{
                            this.updateUsers(room.info().members);
                        });
                        room.on("USER_LEFT",()=>{
                            this.updateUsers(room.info().members);
                        });
                        this.setState({room});
                    })
                })
        })
    }
    componentDidUpdate(prevProps,prevState)
    {
        // should only be state update
        try
        {
            if(this.state.currentChapter.id!==prevState.currentChapter.id)
            {
                let editorjsDesc=this.state.editorjsDesc;
                if(this.state.currentChapter.type===prevState.currentChapter.type)
                {
                    editorjsDesc.blocks.clear();
                    editorjsDesc.render(JSON.parse(this.state.currentChapter.desc));
                }
                else
                {
                    editorjsDesc.destroy();
                    this.setState({editorjsDesc: new EditorJS({
                        holder: this.state.editorjsDescId,
                        data: JSON.parse(this.state.currentChapter.desc),
                        readOnly: true,
                        tools:{
                            header: Header,
                            raw: RawTool,
                            image: SimpleImage,
                            checkList: {
                                class: Checklist,
                                inlineToolbar: true
                            },
                            list: {
                                class: List,
                                inlineToolbar: true,
                                config: {
                                    defaultStyle: 'unordered'
                                }
                            },
                            embed: Embed,
                            quote: Quote
                        }
                    })})
                }
            }
        }
        catch(e)
        {}
    }
    render()
    {
        let displayChapter,chat;
        let course=this.state.course;
        if(course)
        {
            chat=<Widget handleNewUserMessage={this.handleNewUserMessage} title={course.name} subtitle="交流你對課程的想法吧!" senderPlaceHolder="請在此輸入您的訊息" emojis={true}/>
        }
        let chapter=this.state.currentChapter;
        if(chapter)
        {
            if(util.isText(chapter.type))
            {
                displayChapter=<div className="col border">
                    <div style={{height: '100px'}} id={this.state.editorjsDescId}></div>
                </div>
            }
            else // type is implement
            {
                let implement;
                if(util.isAdvancedClass(this.state.course.type))
                {
                    implement=<Editor height={'80%'} defaultLanguage="javascript" onMount={this.handleEditorDidMount}></Editor>
                }
                else
                {
                    let blocks=JSON.parse(this.state.course.blocks);
                    implement=<BlocklyComponent blocks={blocks} onMount={this.handleBlocklyDidMount} className="h-100"/>
                }
                displayChapter=<React.Fragment>
                    <div className="col border">
                        <ul className="nav nav-tabs" role="tablist">
                            <li className="nav-item" role="presentation">
                                <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#desc">說明</button>
                            </li>
                            <li className="nav-item" role="presentation">
                                <button className="nav-link" data-bs-toggle="tab" data-bs-target="#tool" onClick={()=>{
                                    util.resize();
                                }}>程式工具</button>
                            </li>
                            <li className="nav-item me-2">
                                <div className="btn-group">
                                    <button className="btn btn-primary" onClick={this.handleLoad}>加載/重置</button>
                                    <button className="btn btn-primary" onClick={this.handleExecute}>執行</button>
                                </div>
                            </li>
                            <li className="nav-item me-2">
                                <button className="btn btn-primary" onClick={this.handleSync}>同步</button>
                            </li>
                            <li className="nav-item me-2">
                                <button className="btn btn-success" data-bs-toggle="modal" data-bs-target="#inviteModal">邀請</button>
                                <div className="modal fade" id="inviteModal" tabIndex={-1} aria-hidden="true">
                                    <div className="modal-dialog modal-dialog-centered">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5 className="modal-title">在線列表</h5>
                                                <button className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div className="modal-body">
                                                <ul className="list-group">
                                                    {
                                                        this.state.users.map(user=>{
                                                            return <li key={user} className="list-group-item d-flex justify-content-between align-items-center">
                                                                {user}
                                                                <button className="btn btn-primary" onClick={(e)=>{
                                                                    this.inviteClick(e,user)
                                                                }}>邀請</button>
                                                            </li>
                                                        })
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li className="nav-item">
                                <button className="btn btn-danger" data-bs-toggle="modal" data-bs-target="#helpModal">求助</button>
                                <div className="modal fade" id="helpModal" tabIndex={-1} aria-hidden="true">
                                    <div className="modal-dialog modal-dialog-centered">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5 className="modal-title">問題求助</h5>
                                                <button className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div className="modal-body">
                                                <div className="input-group mb-3">
                                                    <span className="input-group-text">求助學生帳號</span>
                                                    <input className="form-control" value={this.context?.user.account} readOnly></input>
                                                </div>
                                                <div className="input-group mb-3">
                                                    <span className="input-group-text">房間ID</span>
                                                    <input className="form-control" value={this.state.roomId} readOnly></input>
                                                </div>
                                                <div className="input-group mb-3">
                                                    <span className="input-group-text">問題簡易描述</span>
                                                    <textarea name="problem_desc" ref={this.problemDesc} className="form-control" placeholder="請在此簡易描述遇到的學習或技術困難，可幫助線上老師提前知悉，並盡快解決!"></textarea>
                                                </div>
                                            </div>
                                            <div className="modal-footer">
                                                <button className="btn btn-secondary" data-bs-dismiss="modal">關閉</button>
                                                <button className="btn btn-primary" onClick={this.sendHelpRequest}>送出求助要求</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <div className="tab-content h-100">
                            <div className="tab-pane fade show active" id="desc">
                                <div style={{height: '100px'}} id={this.state.editorjsDescId}></div>
                            </div>
                            <div className="tab-pane fade h-75" id="tool">
                                {implement}
                            </div>
                        </div>
                    </div>
                    <div className="col border">
                        <div className="h-100" key={this.state.resultKey} id={util.canvasId}>
                            <div ref={this.scriptLocation}></div>
                        </div>
                    </div>
                </React.Fragment>
            }
        }
        return (
            <React.Fragment>
                <div className="container-fluid vh-75">
                    <div className="row h-100">
                        <div className="col border">
                            <div className="list-group">
                                {this.renderTree()}
                            </div>
                        </div>
                        <div className="col-9 border">
                            <div className="row h-75 overflow-auto">
                                {displayChapter}
                            </div>
                            <div className="row h-25">
                                <div className="col border h-100 overflow-auto">
                                    <NoteEditor getCourse={this.getCourse}/>
                                </div>
                                <div className="col border h-100">
                                    <div className="row h-100">
                                        <div className="col w-100 h-100">
                                            <video ref={this.myVideo} className="w-100 h-100" autoPlay playsInline muted></video>
                                        </div>
                                        {
                                            Object.keys(this.state.streamList).map(key=>{
                                                let streamList=this.state.streamList;
                                                return <div key={key} className="col w-100 h-100">
                                                    <Video className="w-100 h-100" srcObject={streamList[key]}></Video>
                                                </div>
                                            })
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {chat}
            </React.Fragment>
        );
    }
}

export default CoursePage;