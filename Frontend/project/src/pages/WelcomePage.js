import React from "react";
import { Link } from "react-router-dom";
import "./WelcomePage.css";
import util from "../util";

class WelcomePage extends React.Component
{
    componentDidMount()
    {
        document.title=util.getTitleByPrefix('首頁');
    }
    render()
    {
        return (
            <div>
                <div className="px-4 py-5 my-auto text-center bg-image background">
                    <div className="container bg-light bg-opacity-80 py-5">
                        <h1 className="display-5 fw-bold">歡迎來到自學程式的起點</h1>
                        <div className="col-lg-6 mx-auto">
                            <p className="lead mb-4">提供配對功能讓學生可以結對學習，相較於獨自設計可以更快的提高個人程式能力並減少挫敗感，增加成就感和樂趣，也促進學生在程式設計上的積極態度。</p>
                            <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                                <button type="button" className="btn btn-primary btn-lg px-4 gap-3">立即加入</button>
                                <Link to="/login">
                                    <button type="button" className="btn btn-outline-secondary btn-lg px-4">登入</button>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container px-4 py-5 my-5" id="features">
                    <h2 className="pb-2 border-bottom" id="featuress">系統特色</h2>

                    <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4 py-5">
                        <div className="col d-flex align-items-start">
                            <i className="bi bi-clock-history icon flex-shrink-0 me-3"></i>
                            <div>
                                <h5 className="fw-bold mb-0">時空限制，消失殆盡</h5>
                                <p>學生可以在任何時間與地點存取教材，時空條件不會成為學習的障礙。</p>
                            </div>
                        </div>

                        <div className="col d-flex align-items-start">
                            <i className="bi bi-calendar-check-fill icon flex-shrink-0 me-3"></i>
                            <div>
                                <h5 className="fw-bold mb-0">進度掌握，由我決定</h5>
                                <p>學生可依據自身現況，如興趣與能力來決定學習進度與排程，並學習如何自學。</p>
                            </div>
                        </div>

                        <div className="col d-flex align-items-start">
                            <i className="bi bi-emoji-laughing-fill icon flex-shrink-0 me-3"></i>
                            <div>
                                <h5 className="fw-bold mb-0">結對學習，共同努力</h5>
                                <p>學生可透過配對系統擁有共同學習的夥伴，並藉由在平台上的互動，增廣見聞。</p>
                            </div>
                        </div>

                        <div className="col d-flex align-items-start">
                            <i className="bi bi-arrow-left-right icon flex-shrink-0 me-3"></i>
                            <div>
                                <h5 className="fw-bold mb-0">及時回饋，提高動機</h5>
                                <p>學生在一般學習上與遇到問題時，都能立即得到系統的回饋，維持學習動機。</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="container px-4 py-5">
                    <h2 className="pb-2 border-bottom" id="users">系統使用對象</h2>
                    <div className="row row-cols-1 row-cols-lg-2 align-items-stretch g-4 py-5">

                        <div className="col">
                            <div className="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" id="student" >
                                <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1 bg-dark bg-opacity-50">
                                    <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold ">學生</h2>
                                    <p>可以選擇自身感興趣並符合自己能力的教材進行學習，在學習中，可任意選擇其他課程。</p>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" id="teacher">
                                <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1 bg-dark bg-opacity-50">
                                    <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">老師</h2>
                                    <p>可以查看正在線上的學生清單，並得知學生當前正在學習的教材狀況，如教材名稱、學習進度、學習時長等，並會在學生按下求助按鈕時，在網頁上顯示通知提醒，教師即可連到學生正在學習的教室，並透過共享螢幕畫面或共同編輯及時查看學生學習遇到的問題，並協助解決。</p>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" id="provider">
                                <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1 bg-dark bg-opacity-50">
                                    <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">教材提供者</h2>
                                    <p>教材提供者可以查看自身製作教材的評價資訊與建議，並藉此做為修正教材的依據更新教材後重新上傳，由於系統會記錄教材上傳歷史，與其對應的相關數據，如每個教材版本的評語、學習次數與放棄次數等，希望藉由提供數據，讓教材提供者可以更好的準備教材，維持平台良好內容。</p>
                                </div>
                            </div>
                        </div>

                        <div className="col">
                            <div className="card card-cover h-100 overflow-hidden text-white bg-dark rounded-5 shadow-lg" id="manager">
                                <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1 bg-dark bg-opacity-50">
                                    <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">系統管理員</h2>
                                    <p>系統管理員可以看到所有正在線上的使用者，並可對使用者資訊進行增刪查改，也可以透過回饋信箱，接收到使用者對於系統的問題與建議，系統管理員可以對此進行回復，並將遇到的問題立即進行修復或排程到系統維護時間進行修復。</p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        );
    }
}

export default WelcomePage;