import React from 'react';
import util from '../util';

class HelpManage extends React.Component
{
    constructor()
    {
        super();
        this.state={
            helps: [],
            curHelp: null,
            statusMsg: ""
        }
    }
    getStatusColor=(status)=>
    {
        switch(status)
        {
            case "未處理":
                return 'text-danger';
            case "正在處理中":
                return 'text-warning';
            case "已處理":
                return 'text-success';
            default:
                return "";
        }
    }
    modifyStatus=(id,status)=>{
        util.modifyHelp(id,status).then(json=>{
            util.getHelps().then(helps=>{
                this.setState({helps});
            })
        });
    }
    componentDidMount()
    {
        util.getHelps().then(json=>{
            this.setState({helps:json});
        })
    }
    render()
    {
        let i=1;
        return <React.Fragment>
        <table className='table'>
            <thead>
                <tr>
                    <th>#</th>
                    <th scope='col'>問題描述</th>
                    <th scope='col'>求助者帳號</th>
                    <th scope='col'>房間連結</th>
                    <th scope='col'>狀態</th>
                    <th scope='col'>操作</th>
                </tr>
            </thead>
            <tbody>
                {
                    this.state.helps.map(help=>{
                        return <tr key={help.id}>
                            <th scope='row'>{i++}</th>
                            <td>{help.problem_desc}</td>
                            <td>{help.user_account}</td>
                            <td><a href={`https://${util.frontend.host}/course?roomId=${help.room_id}`}>連結</a></td>
                            <td className={this.getStatusColor(help.status)}>{help.status}</td>
                            <td>
                                <div className='dropdown'>
                                    <button className='btn btn-primary dropdown-toggle' data-bs-toggle="dropdown" aria-expanded="false"></button>
                                    <ul className='dropdown-menu'>
                                        <li><button className='dropdown-item' onClick={()=>{this.modifyStatus(help.id,util.helpStatus.notProcessed)}}>{util.helpStatus.notProcessed}</button></li>
                                        <li><button className='dropdown-item' onClick={()=>{this.modifyStatus(help.id,util.helpStatus.processing)}}>{util.helpStatus.processing}</button></li>
                                        <li><button className='dropdown-item' onClick={()=>{this.modifyStatus(help.id,util.helpStatus.processed)}}>{util.helpStatus.processed}</button></li>
                                        <li>
                                            <button className='dropdown-item' data-bs-toggle="modal" data-bs-target="#statusModal" onClick={()=>{
                                                this.setState({curHelp:help})
                                            }}>自訂狀態訊息</button>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    })
                }
            </tbody>
        </table>
        <div className='modal fade' id="statusModal" tabIndex={-1} aria-hidden="true">
            <div className='modal-dialog'>
                <div className='modal-content'>
                    <div className='modal-header'>
                        <h5 className='modal-title'>編輯自訂狀態訊息</h5></div>
                    <div className='modal-body'>
                        <input className='form-control' value={this.state.statusMsg} onChange={(e)=>{this.setState({statusMsg:e.target.value})}}></input>
                    </div>
                    <div className='modal-footer'>
                        <button className='btn btn-secondary' data-bs-dismiss="modal">關閉</button>
                        <button className='btn btn-primary' data-bs-dismiss="modal" onClick={()=>{
                            this.modifyStatus(this.state.curHelp.id,this.state.statusMsg);
                        }}>送出</button>
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>
    }
}

export default HelpManage;