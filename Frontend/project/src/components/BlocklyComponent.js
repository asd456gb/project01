/* eslint-disable no-eval */

import React from 'react';
import util from '../util';

import * as Blockly from 'blockly/core';

class BlocklyComponent extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            id: `blockly-${util.newId()}`, //for container
            blockly: null,
            Blockly: Blockly,
            blocks: (props.blocks)?props.blocks:{},
            toolbox: {
                "kind": "flyoutToolbox",
                "contents":[]
            }
        }
    }
    //helper function
    loadBlocks=(blocks,blockly=this.state.blockly)=>
    {
        let empty=true;
        for(let type in blocks)
        {
            empty=false;
            let block=blocks[type];
            if(block==="BuiltIn")
                this.addBuiltInBlock(type,blockly);
            else
                this.addBlock(block.style,block.code,blockly);
        }
        if(empty&&this.props.onSync)
            this.props.onSync();
    }
    loadBlocksByName=(selected_block)=>{
        for(let block of selected_block)
            this.addBuiltInBlock(block);
    }
    clearToolbox=(callback=null)=>{
        let toolbox={
            kind: "flyoutToolbox",
            contents:[]
        };
        this.setState({toolbox},()=>{
            if(callback)
                callback();
        });
    }
    clearBlocks=(value,callback)=>{
        let toolbox={
            kind: "flyoutToolbox",
            contents:[]
        };
        this.setState({blocks: {}},()=>{
            this.setState({toolbox},()=>{
                this.state.blockly.updateToolbox(toolbox);
                callback(value);
            });
        });
    }
    addBlock=(block,code,blockly=this.state.blockly)=>{
        Blockly.Blocks[block.type]={
            init: function(){
                this.jsonInit(block);
            }
        }
        Blockly.JavaScript[block.type]=eval(code);

        let blocks=this.state.blocks;
        blocks[block.type]={
            "style": block,
            "code": code
        }
        this.setState({blocks: blocks},()=>{
            if(this.props.onSync)
                this.props.onSync();
        });

        let toolbox=this.state.toolbox;
        let isNew=true;
        for(let b of toolbox.contents)
        {
            if(b.type===block.type)
            {
                isNew=false;
                break;
            }
        }
        if(isNew)
            toolbox.contents.push({'kind': 'block','type':block.type});
        this.setState({toolbox: toolbox},()=>{
            blockly.updateToolbox(this.state.toolbox);
        });
    }
    addBuiltInBlock=(type,blockly=this.state.blockly)=>{
        let toolbox=this.state.toolbox;
        let isNew=true;
        for(let b of toolbox.contents)
        {
            if(b.type===type)
            {
                isNew=false;
                break;
            }
        }
        if(isNew)
        {
            toolbox.contents.push({'kind': 'block','type':type});
            this.setState({toolbox: toolbox},()=>{
                blockly.updateToolbox(this.state.toolbox);
            });
        }
    }
    deleteBlock=(type,blockly=this.state.blockly)=>{
        let blocks=this.state.blocks;
        delete blocks[type];
        this.setState({blocks: blocks},()=>{
            if(this.props.onSync)
                this.props.onSync();
        });

        let toolbox=this.state.toolbox;
        toolbox.contents=toolbox.contents.filter((block)=>block.type!==type);
        this.setState({toolbox: toolbox},()=>{
            blockly.updateToolbox(this.state.toolbox);
        });
    }
    getBlocks=()=>this.state.blocks;
    componentDidMount()
    {
        let blockly=Blockly.inject(this.state.id,
            {
                toolbox: this.state.toolbox,
                grid:
                {
                    spacing: 20,
                    length: 3,
                    colour: "#ccc",
                    snap: true
                },
                trashcan: true
            }
        );
        this.setState({blockly: blockly});
        if(this.state.blocks)
            this.loadBlocks(this.state.blocks,blockly);
        util.resize();
        if(this.props.onMount)
            this.props.onMount(this);
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(this.props.id!==prevProps.id)
            this.clearBlocks(this.props.blocks?this.props.blocks:{},this.loadBlocks);
    }
    render()
    {
        return (
            <div id={this.state.id} className={this.props.className} style={this.state.style}></div>
        );
    }
}
export default BlocklyComponent;