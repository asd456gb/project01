import React from 'react';
import Editor from '@monaco-editor/react';

import * as Blockly from 'blockly/core';
import util from '../util';
import BlockEditor from './BlockEditor';
//import BlocklyComponent from './BlocklyComponent';

class CourseAdvancedForm extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            currentLink: '',
            fileLinks: [],

            // options
            blockEditor: null,

            // share code
            editor: null
        };
        this.Blockly=Blockly;
    }
    // helper function
    isAdvancedClass=()=>util.isAdvancedClass(this.props.course.type);
    //

    // manage external filelinks
    addLink=(e)=>{
        this.setState({fileLinks: [...this.state.fileLinks,this.state.currentLink]});
        this.setState({currentLink: ""}); // clear currentLink
    }
    deleteLink=(link)=>{
        this.setState({fileLinks: this.state.fileLinks.filter((l)=>{
            return l!==link;
        })});
    }
    currentLinkChange=(e)=>{
        this.setState({currentLink: e.target.value});
    }
    //

    // blockly
    handleBlockEditorDidMount=(blockEditor)=>this.setState({blockEditor});

    // share code
    handleEditorDidMount=(editor,monaco)=>{
        let shareCode=this.props.course.shareCode;
        if(shareCode!==null)
            editor.setValue(shareCode);
        this.setState({editor: editor});
    }
    //

    // submit
    handleSubmit=(e)=>{
        e.preventDefault();

        let data={};
        data['fileLinks']=this.state.fileLinks;

        if(!this.isAdvancedClass())
            data["blocks"]=JSON.stringify(this.state.blockEditor.getBlocks());

        data["shareCode"]=this.state.editor.getValue();

        util.editCourse(data,this.props.course.id)
            .then(async ()=>{
                await this.props.update();
            })
    }
    //
    componentDidMount()
    {
        let fileLinks=JSON.parse(this.props.course.fileLinks);
        fileLinks=(fileLinks==null)?[]:fileLinks;
        this.setState({fileLinks: fileLinks});
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(this.props.course.id!==prevProps.course.id)
        {
            let fileLinks=JSON.parse(this.props.course.fileLinks);
            fileLinks=(fileLinks==null)?[]:fileLinks;
            this.setState({fileLinks: fileLinks});

            let shareCode=this.props.course.shareCode;
            shareCode=(shareCode===null)?"":shareCode;
            this.state.editor.setValue(shareCode);
        }
    }
    render()
    {
        let fileLinks=this.state.fileLinks;
        let isLinksEmpty=fileLinks.length===0;
        let linkItems=null;
        if(isLinksEmpty)
            linkItems=<div className='list-group-item'>目前沒有任何連結</div>;
        else
        {
            linkItems=fileLinks.map(link=>{
                return (
                    <div key={link} className='list-group-item'>
                        <div className='input-group'>
                            <span className='input-group-text form-control'>{link}</span>
                            <button className='btn btn-primary' onClick={
                                ()=>this.deleteLink(link)
                            }>刪除</button>
                        </div>
                    </div>
                )
            })
        }
        let displayBlockly="";
        if(!this.isAdvancedClass())
        {
            displayBlockly=(
                <React.Fragment>
                    <div className='input-group mb-3'>
                        <span className='input-group-text'>積木定義</span>
                        <div className='form-control'>
                            <BlockEditor className="vh-60" style={{width: "100%"}} blocks={JSON.parse(this.props.course.blocks)} id={this.props.course.id} onMount={this.handleBlockEditorDidMount}/>
                        </div>
                    </div>
                </React.Fragment>
            );
        }

        return (
            <form className='p-4' onSubmit={this.handleSubmit}>
                <div className='input-group'>
                    <span className='input-group-text'>外部檔案連結</span>
                    <input type="text" className="form-control" placeholder='請輸入所需外部檔案之連結' value={this.state.currentLink} onChange={this.currentLinkChange}/>
                    <button type="button" className="btn btn-primary" onClick={this.addLink}>新增</button>
                </div>
                <div className='list-group mb-3'>
                    {linkItems}
                </div>
                {displayBlockly}
                <div className='input-group mb-3'>
                    <span className='input-group-text'>所有章節<br/>共用程式碼<br/>(JavaScript)</span>
                    <div className="form-control">
                        <Editor height="60vh" defaultLanguage='javascript' onMount={this.handleEditorDidMount}/>
                    </div>
                </div>
                <div className='d-grid'>
                    <button className='btn btn-primary'>送出</button>
                </div>
            </form>
        )
    }
}

export default CourseAdvancedForm;