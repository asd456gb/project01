import React from 'react';
import { UserContext } from '../context/UserContext';
import util from '../util';

class MaterialBox extends React.Component
{
    static contextType=UserContext;
    constructor()
    {
        super();
        this.state={
            materials: null,
            columnNum: 2
        };
        this.materialInput=React.createRef();
    }
    handleInputChange=(e)=>{
        this.setState({columnNum: e.target.value});
    }
    handleMaterialUpload=(e)=>{
        let file=this.materialInput.current.files[0];
        let formData=new FormData();
        formData.append('name',file.name);
        formData.append('material',file);
        util.uploadMaterial(formData)
            .then(json=>{
                this.updateMaterial();
            })
    }
    updateMaterial=()=>{
        util.getMaterialList(this.context.user.account)
            .then(json=>{
                this.setState({materials: json});
            })
    }
    componentDidMount()
    {
        this.updateMaterial();
    }
    render()
    {
        return (
            <React.Fragment>
                <div className='input-group my-3'>
                    <span className='input-group-text'>上傳教學素材</span>
                    <input type="file" className='form-control' ref={this.materialInput}/>
                    <button className='btn btn-primary' onClick={this.handleMaterialUpload}>上傳</button>
                </div>
                <div className='row'>
                    <div className='col-10'></div>
                    <div className='col'>
                        <div className='input-group'>
                            <span className='input-group-text'>欄位數</span>
                            <input type="number" className='form-control' min={1} max={6} value={this.state.columnNum} onChange={this.handleInputChange}/>
                        </div>
                    </div>
                </div>
                <div className='container border pt-4 pb-4'>
                    <div className={`row row-cols-${this.state.columnNum} g-4`}>
                        {
                            this.state.materials&&this.state.materials.map((material)=>{
                                let link=`https://${util.backend.host}/materials/${material.user_account}/${material.name}`;
                                return (
                                    <div className='col'>
                                        <div key={material.name} className='card'>
                                            <div className='card-body'>
                                                <a href={link} className='card-text' target="_blank" rel="noreferrer">{material.name}</a>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

export default MaterialBox;
