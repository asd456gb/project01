import React from 'react';
import { UserContext } from '../context/UserContext';
import util from '../util';

class PersonalInfoForm extends React.Component
{
    static contextType=UserContext;
    constructor()
    {
        super();
        this.state={
            user:{
                name:"",
                email: "",
                gender: "secret",
                phone: ""
            }
        };
        this.fileInput=React.createRef();
    }
    handleInputChange=(e)=>{
        let user=this.state.user;
        user[e.target.name]=e.target.value;
        this.setState({user});
    }
    handleSubmit=(e)=>
    {
        e.preventDefault();
        console.log(e);
    }
    handleFileUpload=(e)=>{
        let formData=new FormData();
        formData.append('avatar',this.fileInput.current.files[0]);
        util.uploadAvatar(formData)
            .then(json=>{
                console.log(json);
            })
    }
    componentDidMount()
    {
        this.setState({user: this.context.user});
    }
    render()
    {
        return (
            <form className='border p-4' onSubmit={this.handleSubmit}>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>帳號</span>
                    <span className='input-group-text'>{this.context.user.account}</span>
                    <span className='input-group-text'>顯示名稱</span>
                    <input type="text" className="form-control" placeholder='請輸入您希望在系統上顯示的名稱' name="name" value={this.state.user.name} onChange={this.handleInputChange}/>
                    <span className='input-group-text'>性別</span>
                    <select className='form-select' name="gender" value={this.state.user.gender} onChange={this.handleInputChange}>
                        <option value="secret">保密</option>
                        <option value="male">男</option>
                        <option value="female">女</option>
                        <option value="thirdSex">第三性</option>
                    </select>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>電子郵件(E-mail)</span>
                    <input type="text" className="form-control" placeholder='請輸入您的電子郵件，方便其他人聯絡' name="email" value={this.state.user.email} onChange={this.handleInputChange}/>
                    <span className='input-group-text'>個人電話</span>
                    <input type="text" className="form-control" placeholder='請輸入您的個人電話，方便其他人聯絡' name="phone" value={this.state.user.phone} onChange={this.handleInputChange}/>
                </div>
                <div className='d-grid gap-2 mb-3'>
                    <button className='btn btn-primary'>確認</button>
                </div>
                <div className='row d-flex justify-content-center align-items-center'>
                    <div className='col'>
                        <div className='input-group mb-3'>
                            <span className='input-group-text'>個人大頭貼</span>
                            <span className='input-group-text'>
                                <img src="/img/avatar.png" className='img-fluid thumbnail' alt="default avatar"></img>
                            </span>
                        </div>
                    </div>
                    <div className='col'>
                        <div className='input-group mb-3'>
                            <input type="file" className="form-control" ref={this.fileInput}/>
                            <button type="button" className='btn btn-primary' onClick={this.handleFileUpload} >上傳</button>
                        </div>
                    </div>
                </div>
            </form>
        );
    }
}

export default PersonalInfoForm;