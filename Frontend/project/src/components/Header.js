import React from 'react';
import { Link } from "react-router-dom";
import { HashLink } from "react-router-hash-link";
import { UserContext } from '../context/UserContext';

import { Convergence } from "@convergence/convergence";

import util from '../util';

class Header extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            notices: []
        }
        this.logoutClick=this.logoutClick.bind(this);
    }
    logoutClick(e)
    {
        e.preventDefault();
        util.logout()
            .then(json=>{
                console.log(json);
                let { setUser }=this.context;
                setUser(null);
            })
    }
    connectOnline=()=>{
        let { user }=this.context;
        if(!user) return setTimeout(this.connectOnline,500);
        Convergence.connectAnonymously(`https://${util.frontend.convergence}`,user.account).then((domain)=>{
            let chatService=domain.chat();
            chatService.join('online').then(room=>{
                room.on('message',e=>{
                    let json=JSON.parse(e.message);
                    if(json.to===user.account)
                    {
                        let notices=this.state.notices;
                        notices.push(json);
                        this.setState(notices);
                    }
                })
            })
        });
    }
    componentDidMount()
    {
        this.connectOnline();
    }
    render()
    {
        let { user }=this.context;
        let { notices }=this.state;
        let noticeList=<li className='dropdown-item'>當前沒有任何通知</li>;
        if(notices.length)
        {
            noticeList=notices.map(notice=>{
                let content="";
                if(notice.type==="invite")
                {
                    content=<React.Fragment>
                        {`哈囉! ${notice.from} 邀請您加入課程一起學習。`}
                        <a href={`https://${util.frontend.host}/course?roomId=${notice.roomId}`}>連結</a>
                    </React.Fragment>
                }
                return <li key={util.newId()} className='dropdown-item'>{content}</li>
            })
        }
        return (
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/">
                        <img src="/img/logo.png" alt="logo" width="30" height="30" className='d-inline-block align-text-top'></img>
                        {util.title}
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
                            {!user&& (
                                <React.Fragment>
                                    <li className="nav-item">
                                        <HashLink to="/#features" className='nav-link active'>
                                            系統特色
                                        </HashLink>
                                    </li>
                                    <li className="nav-item">
                                        <HashLink to="/#users" className='nav-link active'>
                                            系統使用對象
                                        </HashLink>
                                    </li>
                                </React.Fragment>
                            )}
                            {user&&(
                                <React.Fragment>
                                    <li className='nav-item'>
                                        <i className={`bi bi-bell${(!notices.length)?"":"-fill"} text-light fs-4`} data-bs-toggle="modal" data-bs-target="#notices"></i>
                                        <div className='modal fade' id="notices" tabIndex={-1} aria-hidden="true">
                                            <div className='modal-dialog'>
                                                <div className='modal-content'>
                                                    <div className='modal-header'>
                                                        <h5 className='modal-title'>通知列表</h5>
                                                        <button className='btn-close' data-bs-dismiss="modal" aria-label='Close'></button>
                                                    </div>
                                                    <div className='modal-body'>
                                                        <ul className='list-group'>
                                                            {noticeList}
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li className="nav-item">
                                        <Link to="/dashboard" className='nav-link active'>你好!{user.name}</Link>
                                    </li>
                                    <li>
                                        <a href='logout' onClick={this.logoutClick} className='nav-link active'>登出</a>
                                    </li>
                                </React.Fragment>
                            )}
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
}

Header.contextType=UserContext;

export default Header;