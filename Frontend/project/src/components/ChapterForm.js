/* eslint-disable no-eval */

import React from 'react';
import util from '../util';
import Editor from '@monaco-editor/react';
import BlockSelector from './BlockSelector';
import BlocklyComponent from './BlocklyComponent';
import Interpreter from 'js-interpreter';
import * as Blockly from 'blockly/core';

import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import RawTool from '@editorjs/raw';
import SimpleImage from '@editorjs/simple-image';
import Checklist from '@editorjs/checklist';
import List from '@editorjs/list';
import Embed from '@editorjs/embed';
import Quote from '@editorjs/quote';

class ChapterForm extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state={
            chapter: props.chapter?props.chapter:this.getDefaultChapter(),
            parentChaptersDisplay: <React.Fragment></React.Fragment>,
            //desc
            editor: null,
            editorId: "chapterFormEditor",
            //extra code & validAns
            extraCodeEditor: null,
            validAnsEditor: null,
            // extra block
            blockSelector: null,
            //example & answer (editor & blockly)
            //editor
            exampleEditor: null,
            answerEditor: null,
            //blockly
            exampleBlockly: null,
            answerBlockly: null,
            //preview
            implementEditor: null,
            implementBlockly: null
        }
        this.scriptLocation=React.createRef();
    }
    //helper function
    renderTree=(list,json)=>
    {
        return list.map((chapter)=>{
            return <React.Fragment key={chapter.id}>
                <option value={chapter.id}>
                    {this.getFullRoute(json,chapter)}
                </option>
                {this.renderTree(chapter.childChapters,json)}
            </React.Fragment>
        })
    }
    getFullRoute=(data,chapter)=>{
        let parent=data.find(c=>c.id===chapter.parent_id);
        return parent?`${this.getFullRoute(data,parent)} ➤ ${chapter.name}`:chapter.name;
    }
    getDefaultChapter=()=>{
        return {
            name: "",
            type: "text",
            parent_id: "null",
            order: 0,
            desc: JSON.stringify({}),
            implements: null
        }
    }
    isText=()=>{
        if(!this.state.chapter)
            return true;
        return this.state.chapter.type==="text";
    }
    isNew=()=>{
        return this.props.chapter===null;
    }
    loadEditor=(value,callback)=>{
        let ok=false;
        if(this.state.extraCodeEditor&&this.state.validAnsEditor)
        {
            if(util.isAdvancedClass(this.props.course.type))
            {
                if(this.state.exampleEditor&&this.state.answerEditor)
                    ok=true;
            }
            else
                ok=true;
        }
        if(ok)
            return callback(value);
        return setTimeout(()=>{
            this.loadEditor(value,callback);
        },1000);
    } // recursive function
    //data control
    inputChange=(e)=>{
        let input=e.target;
        let chapter=this.state.chapter;
        chapter[input.name]=input.value;
        this.setState({chapter});
    }
    // when type is implement
    handleExtraCodeEditorDidMount=(editor,monaco)=>this.setState({extraCodeEditor: editor})
    handleValidAnsEditorDidMount=(editor,monaco)=>this.setState({validAnsEditor: editor})
    handleImplementEditorDidMount=(editor,monaco)=>{this.setState({implementEditor: editor})};
    loadImplement=(implement)=>{
        this.state.extraCodeEditor.setValue(implement['extra_code']);
        this.state.validAnsEditor.setValue(implement['valid_answer_code']);
        if(util.isAdvancedClass(this.props.course.type))
        {
            this.state.exampleEditor.setValue(implement['example']);
            this.state.answerEditor.setValue(implement['answer']);
        }
        else
        {
            Blockly.serialization.workspaces.load(implement['example'],this.state.exampleBlockly.state.blockly);
            Blockly.serialization.workspaces.load(implement['answer'],this.state.answerBlockly.state.blockly);
        }
    }
    // example & answer (editor & blockly)
    // editor
    handleExampleEditorDidMount=(editor,monaco)=>this.setState({exampleEditor: editor});
    handleAnswerEditorDidMount=(editor,monaco)=>this.setState({answerEditor : editor});
    handleBlockSelectorDidMount=(blockSelector)=>{
        this.setState({blockSelector});
    }
    handleExampleBlocklyDidMount=(blockly)=>{
        this.setState({exampleBlockly: blockly});
    }
    handleAnswerBlocklyDidMount=(blockly)=>{
        this.setState({answerBlockly: blockly});
    }
    handleImplementBlocklyDidMount=(blockly)=>{
        this.setState({implementBlockly: blockly});
    }
    //
    initApi=(interpreter,globalObject)=>{
        let blocks=JSON.parse(this.props.course.blocks);
        for(let type in blocks)
        {
            let block=blocks[type];
            let functionName=block.style.functionName;
            if(functionName)
                interpreter.setProperty(globalObject,functionName,interpreter.createNativeFunction(window[functionName]));
        }
        let workspace=this.state.implementBlockly.state.blockly;
        interpreter.setProperty(globalObject,'highlightBlock',interpreter.createNativeFunction((id)=>{
            return workspace.highlightBlock(id);
        }));
    }
    nextStep=(interpreter)=>{
        if(interpreter.step())
        {
            if(util.isBasicClass(this.props.course.type))
                this.nextStep(interpreter);
            else
            {
                setTimeout(()=>{
                    this.nextStep(interpreter);
                },100);
            }
        }
        else
        {
            let result=eval(this.state.validAnsEditor.getValue());
            alert((result)?"過關":"沒有過關");
        }
    }
    handleExecute=(e)=>{
        if(util.isAdvancedClass(this.props.course.type))
        {
            let result=eval(this.state.implementEditor.getValue());
            result.then(()=>{
                result=eval(this.state.validAnsEditor.getValue());
                alert((result)?"過關":"沒有過關");
            });
        }
        else if(util.isBasicClass(this.props.course.type))
        {
            let code=Blockly.JavaScript.workspaceToCode(this.state.implementBlockly.state.blockly);
            console.log(code);
            let ftn=`async function f(){${code}} f()`;
            let result=eval(ftn);
            result.then(()=>{
                result=eval(this.state.validAnsEditor.getValue());
                alert((result)?"過關":"沒有過關");
            })
        }
        else // HoC
        {
            let code=Blockly.JavaScript.workspaceToCode(this.state.implementBlockly.state.blockly);
            console.log(code);
            let myInterpreter=new Interpreter(code,this.initApi);
            this.nextStep(myInterpreter);
        }
    }

    getParentChapters=()=>{
        util.getChapters(this.props.course.id)
            .then(json=>{
                json.sort((a,b)=>{
                    return a.order-b.order;
                });

                let hashTable={};
                json.forEach((chapter)=>{
                    hashTable[chapter.id]={...chapter,childChapters:[]};
                });
                let dataTree=[];
                json.forEach((chapter)=>{
                    if(chapter.parent_id)
                        hashTable[chapter.parent_id].childChapters.push(hashTable[chapter.id]);
                    else
                        dataTree.push(hashTable[chapter.id]);
                });

                this.setState({parentChaptersDisplay: <React.Fragment>
                    {this.renderTree(dataTree,json)}
                </React.Fragment>});
            })
    }
    //
    formSubmit=async (e)=>{
        e.preventDefault();
        let formData=new FormData(e.target);
        let desc=JSON.stringify(await this.state.editor.save());
        formData.append("desc",desc);
        if(!this.isText())
        {
            let implement={};
            implement['extra_code']=this.state.extraCodeEditor.getValue();
            implement['valid_answer_code']=this.state.validAnsEditor.getValue();
            if(util.isAdvancedClass(this.props.course.type))
            {
                implement['example']=this.state.exampleEditor.getValue();
                implement['answer']=this.state.answerEditor.getValue();
            }
            else // is not advanced class
            {
                let selected_block=[];
                for(let block in this.state.blockSelector.state.right)
                    selected_block.push(block);
                implement['selected_block']=selected_block;
                implement['example']=Blockly.serialization.workspaces.save(this.state.exampleBlockly.state.blockly);
                implement['answer']=Blockly.serialization.workspaces.save(this.state.answerBlockly.state.blockly);
            }
            formData.append("implements",JSON.stringify(implement));
            console.log(implement);
        }
        formData.append('course_id',this.props.course.id);
        if(!this.props.chapter) // new chapter
        {
            await util.addChapter(formData)
            .then(json=>{
                console.log(json);
            });
        }
        else // modify old chapter
        {
            let data={};
            for(let pair of formData.entries())
                data[pair[0]]=pair[1];
            await util.editChapter(data,this.props.chapter.id)
            .then(json=>{
                console.log(json);
            })
        }
        this.props.update();
    }
    handleLoad=(e)=>{
        e.preventDefault();
        this.setState({previewKey: util.newId()},()=>{
            let div=this.scriptLocation.current;
            div.innerHTML="";
            let script=document.createElement('script');
            script.innerHTML=this.props.course.shareCode;
            div.append(script);
            try
            {
                setTimeout(()=>{
                    let extra_code=JSON.parse(this.state.chapter.implements).extra_code;
                    let script=document.createElement('script');
                    script.innerHTML=extra_code;
                    div.append(script);
                },1000);
            }catch(e){}
            console.log(div);
        })
    }
    handleBlockSync=(selectedBlock)=>{
        let exampleBlockly=this.state.exampleBlockly;
        let answerBlockly=this.state.answerBlockly;
        let implementBlockly=this.state.implementBlockly;
        if(exampleBlockly&&answerBlockly&&implementBlockly)
        {
            exampleBlockly.clearBlocks(selectedBlock,exampleBlockly.loadBlocks);
            answerBlockly.clearBlocks(selectedBlock,answerBlockly.loadBlocks);
            implementBlockly.clearBlocks(selectedBlock,implementBlockly.loadBlocks);
        }
    }
    componentDidMount()
    {
        this.setState({editor: new EditorJS({
            holder: this.state.editorId,
            data: JSON.parse(this.state.chapter.desc),
            tools:{
                header: Header,
                raw: RawTool,
                image: SimpleImage,
                checkList: {
                    class: Checklist,
                    inlineToolbar: true
                },
                list: {
                    class: List,
                    inlineToolbar: true,
                    config: {
                        defaultStyle: 'unordered'
                    }
                },
                embed: Embed,
                quote: Quote
            }
        })},()=>{
            let editor=this.state.editor;
            editor.isReady.then(()=>{
                editor.isReady.isFulfilled=true;
                this.setState({editor: editor});
            })
        })
        this.getParentChapters();
    }
    componentDidUpdate(prevProps,prevState)
    {
        let editor=this.state.editor;
        if(editor.isReady.isFulfilled)
        {
            if(this.props!==prevProps)
            {
                this.setState({chapter: this.props.chapter},()=>{
                    let chapter=this.props.chapter;
                    editor.blocks.clear();
                    if(chapter)
                    {
                        let desc=JSON.parse(chapter.desc);
                        if(desc.blocks.length)
                            editor.blocks.render(desc);
                        if(!this.isText())
                        {
                            let implement=JSON.parse(chapter['implements']);
                            this.loadEditor(implement,this.loadImplement);
                        }
                    }
                    else
                        this.setState({chapter: this.getDefaultChapter()});
                });
                this.getParentChapters();
            }
            else if(this.state!==prevState)
            {
                let chapter=this.state.chapter;
                if(!this.isText())
                {
                    let implement=chapter['implements'];
                    if(implement)
                    {
                        implement=JSON.parse(chapter['implements']);
                        this.loadEditor(implement,this.loadImplement);
                    }
                }
            }
        }
    }
    render()
    {
        let selected_block=null;
        try
        {
            selected_block=JSON.parse(this.state.chapter.implements).selected_block;
        }
        catch(e){}

        let codeWorkspace="";
        if(!this.isText())
        {
            let example,answer,implement,block;
            if(util.isAdvancedClass(this.props.course.type))
            {
                example=<Editor height="40vh" defaultLanguage='javascript' onMount={this.handleExampleEditorDidMount}></Editor>;
                answer=<Editor height="40vh" defaultLanguage='javascript' onMount={this.handleAnswerEditorDidMount}></Editor>;
                implement=<Editor height="50vh" defaultLanguage='javascript' onMount={this.handleImplementEditorDidMount}></Editor>;
            }
            else //block
            {
                example=<BlocklyComponent className="vh-60" style={{width: "100%"}} id={this.state.chapter.id} onMount={this.handleExampleBlocklyDidMount}/>;
                answer=<BlocklyComponent className="vh-60" style={{width: "100%"}} id={this.state.chapter.id} onMount={this.handleAnswerBlocklyDidMount}/>;
                implement=<BlocklyComponent className="vh-50" style={{width: "100%"}} id={this.state.chapter.id} onMount={this.handleImplementBlocklyDidMount}/>;
                block=<div className='input-group mb-3'>
                    <span className='input-group-text'>選擇<br/>章節使用方塊</span>
                    <div className='form-control'>
                        <BlockSelector blocks={JSON.parse(this.props.course.blocks)} onSync={this.handleBlockSync} selected_block={selected_block} onMount={this.handleBlockSelectorDidMount}></BlockSelector>
                    </div>
                </div>
            }
            codeWorkspace=<React.Fragment>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>章節額外程式碼<br/>(JavaScript)</span>
                    <div className="form-control">
                        <Editor height="20vh" defaultLanguage='javascript' onMount={this.handleExtraCodeEditorDidMount}></Editor>
                    </div>
                </div>
                {block}
                <div className='input-group mb-3'>
                    <span className='input-group-text'>驗證答案程式碼<br/>(JavaScript)</span>
                    <div className="form-control">
                        <Editor height="20vh" defaultLanguage='javascript' onMount={this.handleValidAnsEditorDidMount}></Editor>
                    </div>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>範例程式</span>
                    <div className='form-control'>{example}</div>
                    <span className='input-group-text'>參考答案</span>
                    <div className='form-control'>{answer}</div>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>預覽</span>
                    <button className='btn btn-secondary dropdown-toggle dropdown-toggle-split' data-bs-toggle="dropdown" aria-expanded="false" style={{backgroundColor: '#e9ecef',borderColor: "#ced4da"}}></button>
                    <ul className='dropdown-menu'>
                        <li className='dropdown-item'>
                            <a href=" " className='dropdown-item' onClick={this.handleLoad}>加載</a>
                        </li>
                    </ul>
                    <div className='form-control'>
                        <div className='input-group'>
                            <span className='input-group-text'>執行畫面</span>
                            <div className='form-control vh-60' key={this.state.previewKey} id={util.canvasId}></div>
                            <span className='input-group-text'>實作畫面</span>
                            <div className='form-control'>
                                {implement}
                                <div className='d-grid gap-2 mt-4'>
                                    <button className='btn btn-primary' type='button' onClick={this.handleExecute}>執行</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ref={this.scriptLocation}></div>
                </div>
            </React.Fragment>;
        }
        let chapter=this.state.chapter;
        if(!chapter) // chapter is set to default value
            chapter=this.getDefaultChapter();
        return (
            <form className='p-4' onSubmit={this.formSubmit}>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>章節名稱</span>
                    <input className='form-control' placeholder='請輸入章節名稱(必填)' name="name" value={chapter.name} onChange={this.inputChange}/>
                    <span className='input-group-text'>章節類型</span>
                    <select className='form-select' name="type" value={chapter.type} onChange={this.inputChange}>
                        {Object.entries(util.chapterType).map(value=>{
                            return <option key={value[0]} value={value[0]}>{value[1]}</option>
                        })}
                    </select>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>上級章節</span>
                    <select className='form-select' name="parent_id" value={chapter.parent_id?chapter.parent_id:"null"} onChange={this.inputChange}>
                        <option value="null">無</option>
                        {this.state.parentChaptersDisplay}
                    </select>
                    <span className='input-group-text'>順序</span>
                    <input type="number" className='form-control' placeholder='請輸入順序(由小到大)' name="order" value={chapter.order} onChange={this.inputChange}/>
                </div>
                <div className='input-group mb-3'>
                    <span className='input-group-text'>章節說明</span>
                    <div className='form-control' id={this.state.editorId}></div>
                </div>
                {codeWorkspace}
                <div className='d-grid'>
                    <button className='btn btn-primary'>
                        {(this.isNew())?'新增':'修改'}
                    </button>
                </div>
            </form>
        );
    }

}

export default ChapterForm;