import React from 'react';
import util from '../../util';

import "./CommentItem.css";

class CommentItem extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            status: this.props.reaction, // true: like, false: like, null: not any reaction
            good_num: this.props.good_num,
            bad_num: this.props.bad_num,
            reply: "",
            replies: this.props.reply
        };
    }
    handleLikeOrDisLike=(e)=>{
        let what=e.target.dataset.type;
        let operation="";
        let status=this.state.status;
        let good_num=this.state.good_num;
        let bad_num=this.state.bad_num;
        if(what==='like')
        {
            if(status===true)
            {
                operation="cancel";
                status=null;
                good_num--;
            }
            else // status===false or null
            {
                good_num++;
                bad_num=status===false?bad_num-1:bad_num;
                status=true;
                operation="like";
            }
        }
        else // what === dislike
        {
            if(status===false)
            {
                operation="cancel";
                status=null;
                bad_num--;
            }
            else // status===true or null
            {
                bad_num++;
                good_num=status===true?good_num-1:good_num;
                status=false;
                operation="dislike";
            }
        }
        util.makeReaction(this.props.id,operation)
            .then(json=>{
                console.log(json);
            });
        this.setState({status,good_num,bad_num});
    }
    replyChange=(e)=>{
        this.setState({reply:e.target.value});
    }
    addReply=async (e)=>{
        let formData=new FormData();
        formData.append("content",this.state.reply);
        formData.append("course_id",this.props.course_id);
        formData.append("parent_id",this.props.id);
        let json=await util.addComment(formData);
        json.good_num=0;
        json.bad_num=0;
        let replies=this.state.replies;
        replies.push(json);
        this.setState({reply:"",replies});
    }
    render()
    {
        const stars=Array.from(Array(5).keys()).reverse();
        let reply="";
        let replies="";
        let star="";
        if(this.state.replies&&this.state.replies.length)
        {
            reply=<button className='btn ms-3' data-bs-toggle="collapse" data-bs-target={`#replies-${this.props.id}`}>{this.state.replies.length} 則回覆</button>;
            replies=this.state.replies.map(r=>{
                return <div key={r.id} className='my-1'>
                    <CommentItem {...r} />
                </div>
            });
        }
        if(this.props.star)
        {
            star=<fieldset className="rating-fixed">
                    {
                        stars.map((star)=>{
                            let full=(star+1)*2;
                            let half=full-1;
                            let id=(x)=>`star-${x}-${this.props.id}`;
                            return <React.Fragment key={`${full}-${half}`}>
                                <input type="radio" name={`rating-${this.props.id}`} id={id(full)} value={full} checked={this.props.star===full} readOnly/>
                                <label className="full" htmlFor={id(full)}></label>
                                <input type="radio" name={`rating-${this.props.id}`} id={id(half)} value={half} checked={this.props.star===half} readOnly/>
                                <label className="half" htmlFor={id(half)}></label>
                            </React.Fragment>
                        })
                    }
                </fieldset>
        }
        return (
            <div className='card'>
                <div className='card-body'>
                    <div className='mb-1'>
                        {this.props.user_account?this.props.user_account:"測試者"}
                        {star}
                    </div>
                    <p className='card-text'>{this.props.content}</p>
                    <div>
                        <i className="fa fa-thumbs-up likebtn" data-type="like" style={this.state.status===true?{color:"#3f51b5"}:{}} onClick={this.handleLikeOrDisLike}></i> {this.state.good_num}
                        <i className="fa fa-thumbs-down ms-3 dislikebtn" data-type="dislike" style={this.state.status===false?{color:"#3f51b5"}:{}} onClick={this.handleLikeOrDisLike}></i> {this.state.bad_num}
                        {reply}
                        <button className='btn btn-outline-primary ms-3' data-bs-toggle="collapse" data-bs-target={`#reply-${this.props.id}`}>回覆</button>
                        <div className='collapse mt-2' id={`reply-${this.props.id}`}>
                            <input type="text" className='form-control mb-2' placeholder='新增回覆...' value={this.state.reply} onChange={this.replyChange}/>
                            <div className='text-end'>
                                <button className='btn' data-bs-toggle="collapse" data-bs-target={`#reply-${this.props.id}`}>取消</button>
                                <button className='btn btn-primary' onClick={this.addReply}>回覆</button>
                            </div>
                        </div>
                        <div className='collapse mt-2' id={`replies-${this.props.id}`}>
                            {replies}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CommentItem;