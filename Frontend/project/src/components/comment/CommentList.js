import React from 'react';
import util from '../../util';
import CommentItem from './CommentItem';

class CommentList extends React.Component
{
    constructor(props)
    {
        super(props)
        this.state={
            comments: [],
            currentPage: 1,
            links: [],
        };
    }
    getComments=(currentPage=1)=>{
        util.getComments(this.props.course.id,currentPage)
            .then(json=>{
                let links=json.links;
                for(let link of links)
                {
                    if(link.url)
                    {
                        let url=new URL(link.url);
                        link.page=url.searchParams.get('page');
                    }
                    if(link.label.indexOf('Previous')>=0)
                        link.label='«';
                    else if(link.label.indexOf('Next')>=0)
                        link.label='»';
                }
                this.setState({comments: json.data,links: links});
            });
    }
    handlePageClick=(e)=>{
        this.getComments(e.target.name);
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(prevProps.course.id!==this.props.course.id)
            this.getComments();
    }
    render()
    {
        return ( <React.Fragment>
            <div>
                {
                    this.state.comments.map((comment)=>{
                        return <React.Fragment key={comment.id}>
                            <div className='my-2'>
                                <CommentItem {...comment}/>
                            </div>
                        </React.Fragment>
                    })
                }
            </div>
            <nav className='pagination justify-content-center mt-3'>
                {
                    this.state.links.map((link)=>{
                        let disabled=link.page?'':'disabled';
                        let active=link.active?'active':'';
                        return (
                            <li key={link.label} className={`page-item ${disabled} ${active}`}>
                                <button className='page-link' name={link.page} onClick={this.handlePageClick}>{link.label}</button>
                            </li>
                        )
                    })
                }
            </nav>
        </React.Fragment>
        );
    }
}

export default CommentList;