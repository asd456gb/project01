import React from 'react';
import util from '../../util';
import CommentList from './CommentList';

import "./CommentBox.css";

class CommentBox extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            star:0,
            comment:"",
            lastStarRadio: null,
            canComment: false
        }
        this.star=React.createRef();
        this.commentArea=React.createRef();
    }
    starChange=(e)=>{
        this.setState({lastStarRadio: e.target});
        this.setState({star: e.target.value});
    }
    commentChange=(e)=>this.setState({comment: e.target.value});
    sendComment=(e)=>{
        let formData=new FormData();
        formData.append("content",this.state.comment);
        formData.append("star",this.state.star);
        formData.append("course_id",this.props.course.id);
        util.addComment(formData)
            .then((json)=>{
                console.log(json);
            });
        this.resetRating();
    }
    resetRating=()=>{
        this.setState((state)=>{
            let radio=this.state.lastStarRadio;
            if(radio)
                radio.checked=false;
            return {
                comment:"",
                star:0,
                lastStarRadio: null
            }
        });
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(prevProps.course.id!==this.props.course.id)
        {
            this.resetRating();
            util.canComment(this.props.course.id)
            .then(json=>{
                this.setState({canComment:json.canComment});
            });
        }
    }
    render()
    {
        let msg=(this.state.canComment)?"可以在此處輸入你的評論":"您已經留過言了，無法再次留言";

        const stars=Array.from(Array(5).keys()).reverse();
        return ( <React.Fragment>
            <div className='my-3'>
                <CommentList course={this.props.course}/>
            </div>
            <div className='input-group my-3'>
                <span className='input-group-text'>
                    <fieldset className="rating">
                        {
                            stars.map((star)=>{
                                let full=(star+1)*2;
                                let half=full-1;
                                let id=(x)=>`star-${x}`;
                                return <React.Fragment key={`${full}-${half}`}>
                                    <input type="radio" name="rating" id={id(full)} value={full} onChange={this.starChange}/>
                                    <label className="full" htmlFor={id(full)}></label>
                                    <input type="radio" name="rating" id={id(half)} value={half} onChange={this.starChange}/>
                                    <label className="half" htmlFor={id(half)}></label>
                                </React.Fragment>
                            })
                        }
                    </fieldset>
                </span>
                <textarea placeholder={msg} className='form-control' value={this.state.comment} onChange={this.commentChange} disabled={!this.state.canComment}></textarea>
                <button className='btn btn-primary' onClick={this.sendComment} disabled={!this.state.canComment}>送出</button>
            </div> </React.Fragment>
        );
    }
}

export default CommentBox;