import React from 'react';

import EditorJS from '@editorjs/editorjs';
import Header from '@editorjs/header';
import RawTool from '@editorjs/raw';
import SimpleImage from '@editorjs/simple-image';
import Checklist from '@editorjs/checklist';
import List from '@editorjs/list';
import Embed from '@editorjs/embed';
import Quote from '@editorjs/quote';

import { Convergence } from '@convergence/convergence';

import util from '../util';
import CommentBox from './comment/CommentBox';

class MyCourse extends React.Component
{
    constructor()
    {
        super();
        this.state={
            courses: null,

            columnNum: 4,

            page: null,
            perPage: 8,
            links: null,

            currentCourse: {
                type: "HoC",
                min_age: "",
                max_age: "",
                user_account: "",
            },
            editor: null,
            editorId: `courseDescEditor-${util.newId()}`
        };
    }
    handleInputChange=(e)=>{
        let input=e.target;
        let data={};
        data[input.name]=input.value;
        this.setState(data,()=>{
            if(input.name==='perPage')
                this.updateCourse();
        });
    }
    handlePageClick=(e)=>{
        let page=e.target.name;
        this.setState({page},()=>{
            this.updateCourse();
        })
    }
    handleCourseClick=(e)=>{
        Convergence.connectAnonymously(`https://${util.frontend.convergence}`)
            .then(async (domain)=>{
                const modelService=domain.models();
                let initModel=await modelService.create(
                    {
                        collection: 'co-work',
                        data: {
                            course_id: this.state.currentCourse.id,
                            data: ""
                        }
                    }
                );
                await domain.chat().create(
                    {
                        id: initModel,
                        type: 'room',
                        membership: 'public'
                    }
                );
                return initModel;
            }).then((roomId)=>{
                window.location.replace(`/course?roomId=${roomId}`);
            })
    }
    updateCourse=()=>{
        let params={
            perPage: this.state.perPage
        };
        if(this.state.page)
            params.page=this.state.page;
        util.displayCourses(params)
            .then(json=>
            {
                this.setState({page: json.current_page});
                this.setState({courses: json.data});

                let links=json.links;
                for(let link of links)
                {
                    if(link.url)
                    {
                        let url=new URL(link.url);
                        link.page=url.searchParams.get('page');
                    }
                    if(link.label.indexOf('Previous')>=0)
                        link.label='«';
                    else if(link.label.indexOf('Next')>=0)
                        link.label='»';
                }
                this.setState({links});
            }
        );
    }
    loadEditor=()=>{
        let editor=this.state.editor;
        editor.blocks.clear();
        try
        {
            let json=JSON.parse(this.state.currentCourse.desc);
            if(json.blocks.length!==0)
                editor.blocks.render(json);
        }
        catch(e){}
    }
    componentDidMount()
    {
        this.updateCourse();
        this.setState({editor: new EditorJS({
            holder: this.state.editorId,
            data: (this.state.currentCourse)?{}:JSON.parse(this.state.currentCourse.desc),
            placeholder: "此課程無詳細說明。",
            readOnly: true,
            tools:{
                header: Header,
                raw: RawTool,
                image: SimpleImage,
                checkList: {
                    class: Checklist,
                    inlineToolbar: true
                },
                list: {
                    class: List,
                    inlineToolbar: true,
                    config: {
                        defaultStyle: 'unordered'
                    }
                },
                embed: Embed,
                quote: Quote
            },
            onReady: ()=>{
                let editor=this.state.editor;
                editor.isReady=true;
                this.loadEditor();
            }
        })});
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(prevState!==this.state)
        {
            let editor=this.state.editor;
            if(editor.isReady===true)
                this.loadEditor();
        }
    }
    render()
    {
        let course=this.state.currentCourse;
        return (
            <div className='container border mt-4'>
                <div className='row my-3'>
                    <div className='col-8'></div>
                    <div className='col'>
                        <div className='row'>
                            <div className='col'>
                                <div className='input-group'>
                                    <span className='input-group-text'>每頁</span>
                                    <input type='number' name='perPage' className='form-control' min={1} max={20} value={this.state.perPage} onChange={this.handleInputChange}/>
                                    <span className='input-group-text'>條</span>
                                </div>
                            </div>
                            <div className='col'>
                                <div className='input-group'>
                                    <span className='input-group-text'>欄位數</span>
                                    <input type="number" name='columnNum' className='form-control' min={1} max={6} value={this.state.columnNum} onChange={this.handleInputChange}/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={`row row-cols-${this.state.columnNum} g-4`}>
                    {
                        this.state.courses&&this.state.courses.map((course)=>{
                            return (
                                <div className='col' key={course.id}>
                                    <div className='card'>
                                        <div className='card-body'>
                                            <h5 className='card-title'>{course.name}</h5>
                                            <p className='card-text'>{course.intro}</p>
                                        </div>
                                        <ul className='list-group list-group-flush'>
                                            <li className='list-group-item'>課程類型: {util.courseType[course.type]}</li>
                                            <li className='list-group-item'>適合年齡: {`${course.min_age}歲 ~ ${course.max_age}歲`}</li>
                                        </ul>
                                        <button className='btn btn-primary' data-bs-toggle='modal' data-bs-target="#displayCourseModal" onClick={()=>{
                                            this.setState({currentCourse: course})
                                        }}>查看詳情</button>
                                    </div>
                                </div>
                            );
                        })
                    }
                </div>
                <nav className='mt-3'>
                    <ul className='pagination justify-content-center'>
                        {
                            this.state.links&&this.state.links.map(link=>{
                                let disabled=link.page?'':'disabled';
                                let active=link.active?'active':'';
                                return (<li key={link.label} className={`page-item ${disabled} ${active}`}>
                                    <button className='page-link' name={link.page} onClick={this.handlePageClick}>{link.label}</button>
                                </li>)
                            })
                        }
                    </ul>
                </nav>
                <div className='modal fade' id="displayCourseModal" tabIndex={-1} aria-hidden="true">
                    <div className='modal-dialog modal-lg modal-dialog-centered'>
                        <div className='modal-content'>
                            <div className='modal-header'>
                                <h5 className='modal-title'>{course.name}</h5>
                                <button type="button" className='btn-close' data-bs-dismiss='modal' aria-label="Close"></button>
                            </div>
                            <div className='modal-body'>
                                <div className='row g-2 mb-3'>
                                    <div className='col'>
                                        <div className='form-floating'>
                                            <input type="text" className='form-control' id="courseType" value={util.courseType[course.type]} readOnly/>
                                            <label htmlFor='courseType'>課程類型</label>
                                        </div>
                                    </div>
                                    <div className='col'>
                                        <div className='form-floating'>
                                            <input type="text" className='form-control' id="age" value={`${course.min_age}歲 ~ ${course.max_age}歲`} readOnly/>
                                            <label htmlFor='age'>課程適合年齡</label>
                                        </div>
                                    </div>
                                    <div className='col'>
                                        <div className='form-floating'>
                                            <input type="text" className='form-control' id="courseProvider" value={course.user_account} readOnly/>
                                            <label htmlFor='coursePovider'>課程提供者</label>
                                        </div>
                                    </div>
                                </div>
                                <div className='form-floating mb-3'>
                                    <textarea className='form-control' id="courseIntro" value={course.intro} readOnly style={{height: '100px'}}></textarea>
                                    <label htmlFor='courseIntro'>課程簡介</label>
                                </div>
                                <ul className='nav nav-tabs nav-fill' role='tablist'>
                                    <li className='nav-item' role='presentation'>
                                        <button className='nav-link active' data-bs-toggle='tab' data-bs-target='#courseDesc'>課程詳細說明</button>
                                    </li>
                                    <li className='nav-item' role='presentation'>
                                        <button className='nav-link' data-bs-toggle='tab' data-bs-target='#courseChapters'>課程章節安排</button>
                                    </li>
                                    <li className='nav-item' role='presentation'>
                                        <button className='nav-link' data-bs-toggle='tab' data-bs-target='#courseComments'>課程相關評論</button>
                                    </li>
                                </ul>
                                <div className='tab-content'>
                                    <div className='tab-pane fade show active' id="courseDesc">
                                        <div id={this.state.editorId}></div>
                                    </div>
                                    <div className='tab-pane fade' id='courseChapters'>

                                    </div>
                                    <div className='tab-pane fade' id='courseComments'>
                                        <CommentBox course={this.state.currentCourse}/>
                                    </div>
                                </div>
                                <div className='d-grid'>
                                    <button className='btn btn-primary' onClick={this.handleCourseClick}>上課去</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MyCourse;