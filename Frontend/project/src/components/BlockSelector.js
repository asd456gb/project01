import React from 'react';
import util from '../util';

class BlockSelector extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            left: props.blocks?props.blocks:{},
            right: {},
            leftSelected: [],
            rightSelected: []
        };
        if(this.props.onSync)
            this.props.onSync(this.state.right);
        if(this.props.onMount)
            this.props.onMount(this);
    }
    handleSelect=(e,side)=>{
        let name=e.target.getAttribute('name');
        let selected=(side==='left')?this.state.leftSelected:this.state.rightSelected;
        let index=selected.indexOf(name);
        if(index!==-1)
            selected.splice(index,1);
        else
            selected.push(name);
        if(side==='left')
            this.setState({leftSelected: selected});
        else
            this.setState({rightSelected: selected});
    }
    handleToOtherSide=(e,side)=>{
        let from=(side==='left')?this.state.left:this.state.right;
        let to=(side==='left')?this.state.right:this.state.left;
        let selected=(side==='left')?this.state.leftSelected:this.state.rightSelected;
        for(let element of selected)
        {
            to[element]=from[element];
            delete from[element];
        }
        if(side==='left') // To right
        {
            this.setState({leftSelected: []});
            this.setState({left: from});
            this.setState({right: to},()=>{
                if(this.props.onSync)
                    this.props.onSync(this.state.right);
            });
        }
        else // side is right - To left
        {
            this.setState({rightSelected: []});
            this.setState({left: to});
            this.setState({right: from},()=>{
                if(this.props.onSync)
                    this.props.onSync(this.state.right);
            });
        }
    }
    addBuiltInBlock=(e)=>{
        let right=this.state.right;
        console.log(right);
        right[e.target.value]="BuiltIn";
        this.setState({right},()=>{
            if(this.props.onSync)
                this.props.onSync(this.state.right);
        });
    }
    loadSelectedBlock=()=>{
        try
        {
            let selected_block=this.props.selected_block;
            let left=this.state.left;
            let right=this.state.right;
            for(let block of selected_block)
            {
                if(left[block])
                {
                    right[block]=left[block];
                    delete left[block];
                }
                else // built-in
                    right[block]="BuiltIn";
            }
            this.setState({left},()=>{
                this.setState({right},()=>{
                    if(this.props.onSync)
                        this.props.onSync(this.state.right);
                })
            })
        }
        catch(e){}
    }
    componentDidMount()
    {
        this.loadSelectedBlock();
    }
    render()
    {
        let left=this.state.left;
        let right=this.state.right;
        return <React.Fragment>
            <div className='row align-items-center justify-content-center'>
                <div className='col-md-5'>
                    <div>待選</div>
                    <div className='text-end border vh-20'>
                        <ul className='list-group'>
                            {
                                Object.keys(left).map(name=>{
                                    let index=this.state.leftSelected.indexOf(name);
                                    let className='list-group-item list-group-item-action '+((index===-1)?'':'active');
                                    return <li key={name} className={className} onClick={(e)=>this.handleSelect(e,'left')} name={name}>{name}</li>
                                })
                            }
                        </ul>
                    </div>
                </div>
                <div className='col-md-1'>
                        <button className='btn btn-outline-secondary btn-sm' type="button" onClick={(e)=>{this.handleToOtherSide(e,'right')}}>
                            <i className='bi bi-arrow-bar-left'></i>
                        </button>
                        <div></div>
                        <button className='btn btn-outline-secondary btn-sm' type="button" onClick={(e)=>{this.handleToOtherSide(e,'left')}}>
                            <i className='bi bi-arrow-bar-right'></i>
                        </button>
                </div>
                <div className='col-md-5'>
                    <div>
                        <div className='dropend'>
                            已選
                            <a className='ms-3 btn btn-outline-secondary btn-sm dropdown-toggle' href=' ' role="button" data-bs-toggle="dropdown"  aria-expanded="false">加入內建方塊</a>
                            <ul className='dropdown-menu'>
                                <li>
                                    <select className='form-select form-select-sm' onChange={this.addBuiltInBlock}>
                                        <option value="defalut">請選擇要加入的方塊</option>
                                        {
                                            util.getDefaultBasicToolbox().contents.map((element)=>{
                                                if(element.kind==="category")
                                                {
                                                    let contents=element.contents;
                                                    if(contents)
                                                    {
                                                        return <optgroup key={element.name} label={element.name}>
                                                            {
                                                                contents.map(block=>{
                                                                    return <option key={block.type} value={block.type}>{block.type}</option>
                                                                })
                                                            }
                                                        </optgroup>
                                                    }
                                                }
                                                return '';
                                            })
                                        }
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className='text-start border vh-20'>
                        <ul className='list-group'>
                            {
                                Object.keys(right).map(name=>{
                                    let index=this.state.rightSelected.indexOf(name);
                                    let className='list-group-item list-group-item-action '+((index===-1)?'':'active');
                                    return <li key={name} className={className} onClick={(e)=>this.handleSelect(e,'right')} name={name}>{name}</li>
                                })
                            }
                        </ul>
                    </div>
                </div>
            </div>
        </React.Fragment>
    }
}

export default BlockSelector;