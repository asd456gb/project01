import React from 'react';
import SimpleMdeReact from 'react-simplemde-editor';
import "easymde/dist/easymde.min.css";
import util from '../../util';

class NoteEditor extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            status: null,
            title: "",
            content: "",
            course_id: "",
            note_id: "",
            noteList :[]
        };
    }
    statusChange=(status)=>{
        this.setState({status});
    }
    editMode=(note)=>{
        this.setState({
            title: note.title,
            content: note.content,
            note_id: note.id
        });
    }
    titleChange=(e)=>{
        let title=e.target.value;
        this.setState({title});
    }
    contentChange=(content)=>{
        this.setState({content});
    }
    save=()=>{
        let title=this.state.title;
        let content=this.state.content;
        if(this.state.status==="add")
        {
            let course_id=this.props.getCourse().id;
            let formData=new FormData();
            formData.append("title",title);
            formData.append("content",content);
            formData.append("course_id",course_id);
            util.addNote(formData)
                .then(json=>{
                    console.log(json);
                    this.statusChange(null);
                })
        }
        else if(this.state.status==='edit')
        {
            let note={
                title: title,
                content: content
            };
            util.modifyNote(this.state.note_id,note)
                .then(json=>{
                    console.log(json);
                    this.statusChange(null);
                })
        }
    }
    render()
    {
        let display=<div className='d-flex flex-column h-100 justify-content-center align-items-center'>
            <button className='btn btn-primary btn-lg mb-3' onClick={()=>{
                this.statusChange('add');
                this.editMode({
                    title: "",
                    content: "",
                    note_id: ""
                });
                }}>新增筆記</button>
            <button className='btn btn-success btn-lg' onClick={()=>{
                this.statusChange('modify');
                util.getNotes(this.props.getCourse().id)
                    .then(json=>{
                        console.log(json);
                        this.setState({noteList:json});
                    })
            }}>編輯筆記</button>
        </div>
        if(this.state.status==="add"||this.state.status==='edit')
        {
            display=<React.Fragment>
                <div className="input-group">
                    <span className='input-group-text'>標題</span>
                    <input type="text" className='form-control' placeholder='請輸入筆記標題' value={this.state.title} onChange={this.titleChange}/>
                    <button className='btn btn-success' onClick={this.save}>儲存</button>
                    <button className='btn btn-primary' onClick={()=>{
                        this.statusChange(null);
                    }}>返回</button>
                </div>
                <div className='h-70 overflow-auto'>
                    <SimpleMdeReact value={this.state.content} onChange={this.contentChange}/>
                </div>
            </React.Fragment>
        }
        else if(this.state.status==="modify")
        {
            display=<React.Fragment>
                <button className='btn btn-primary' onClick={()=>{
                        this.statusChange(null);
                    }}>返回</button>
                <ol className='list-group list-group-numbered mt-2 overflow-auto'>
                    {this.state.noteList.map(note=>{
                        return <li className='list-group-item d-flex justify-content-between align-items-start'>
                            <div className='ms-2 me-auto'>
                                <div className='fw-bold'>{note.title}</div>
                                {note.updated_at}
                            </div>
                            <button className='btn btn-success' onClick={()=>{
                                this.statusChange("edit");
                                this.editMode(note);
                            }}>編輯</button>
                        </li>
                    })
                    }
                </ol>
            </React.Fragment>
        }
        return (
            <React.Fragment>
                {display}
            </React.Fragment>
        );
    }
};

export default NoteEditor;