import React from 'react';
import HTMLFlipBook from 'react-pageflip';
import ReactMarkdown from 'react-markdown';
import util from '../util';

class MyNote extends React.Component
{
    constructor()
    {
        super();
        this.state={
            notedCourses: [],
            notes: []
        };
    }
    componentDidMount()
    {
        util.getNotedCourses()
            .then(json=>{
                this.setState({notedCourses:json});
            })
    }
    render()
    {
        let book=<HTMLFlipBook width={300} height={500} size="stretch">
            {this.state.notes.map(note=>{
                return <div className='border'>
                    <div className="input-group">
                        <span className='input-group-text'>標題</span>
                        <input type="text" className='form-control' placeholder='請輸入筆記標題' value={note.title}/>
                    </div>
                    <div className='m-2'>
                        <ReactMarkdown>{note.content}</ReactMarkdown>
                    </div>
                </div>
            })}
        </HTMLFlipBook>
        return <React.Fragment>
            <div className='row'>
                <div className='col vh-80 m-4'>
                    <div className='list-group'>
                        {this.state.notedCourses.map(course=>{
                            return <button className='list-group-item list-group-item-action' onClick={()=>{
                                util.getNotes(course.id)
                                    .then(json=>{
                                        console.log(json);
                                        this.setState({
                                            notes: json
                                        })
                                    })
                            }}>{course.name}</button>
                        })}
                    </div>
                </div>
                <div className='col-9'>
                    <div className='mt-3'>
                        {book}
                    </div>
                </div>
            </div>
        </React.Fragment>;
    }
};

export default MyNote;