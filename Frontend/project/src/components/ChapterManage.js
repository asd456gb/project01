import React from 'react';
import util from '../util';
import ChapterForm from './ChapterForm';

class ChapterManage extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            chapters: null,
            currentChapter: null
        };
    }
    setCurrentChapter=(currentChapter)=>this.setState({currentChapter});
    updateChapters=()=>{
        util.getChapters(this.props.course.id)
            .then(chapters=>{
                chapters.sort((a,b)=>{
                    return a.order-b.order;
                });

                let hashTable={};
                chapters.forEach((chapter)=>{
                    hashTable[chapter.id]={...chapter,childChapters:[]};
                });
                let dataTree=[];
                chapters.forEach((chapter)=>{
                    if(chapter.parent_id)
                        hashTable[chapter.parent_id].childChapters.push(hashTable[chapter.id]);
                    else
                        dataTree.push(hashTable[chapter.id]);
                });

                this.setState({chapters: dataTree});
            });
    }
    renderTree=(list,depth=1)=>
    {
        return list.map((chapter)=>{
            let display=chapter.childChapters.length===0?'none':'unset';

            let id=`collapse-${chapter.id}`;
            return <React.Fragment key={chapter.id}>
                <button className='list-group-item list-group-item-action' style={{textIndent: (depth-1)*1.5+"em"}} onClick={()=>{
                        this.setCurrentChapter(chapter);
                    }}>
                        <div className='form-check form-switch'>
                            <input className='form-check-input' type="checkbox" role="switch" data-bs-toggle="collapse" data-bs-target={`#${id}`} aria-expanded="true" style={{display: display}}></input>
                            <label className="form-check-label">{chapter.name}</label>
                        </div>
                </button>
                <div className="collapse show" id={id}>
                    {this.renderTree(chapter.childChapters,depth+1)}
                </div>
            </React.Fragment>
        })
    }
    componentDidMount()
    {
        this.updateChapters();
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(this.props.course.id!==prevProps.course.id)
        {
            this.updateChapters();
            this.setState({currentChapter: null});
        }
    }
    render()
    {
        let course=this.props.course;
        let chapters=this.state.chapters?this.state.chapters:[];
        return (
                    <div className='row'>
                        {/* sidebar */}
                        <div className='col vh-80 m-4'>
                            <div className='d-grid'>
                                <button className='btn btn-primary' onClick={()=>{
                                    this.setCurrentChapter(null);
                                }}>新增章節</button>
                                {/* collpase */}
                            </div>
                            <div className='list-group'>
                                {this.renderTree(chapters)}
                            </div>
                        </div>
                        <div className='col-9'>
                            <ChapterForm course={course} chapter={this.state.currentChapter} update={this.updateChapters}/>
                            {(this.state.currentChapter)?this.state.currentChapter.id:"none"}
                        </div>
                    </div>
            // chapter edit
        );
    }
}

export default ChapterManage;