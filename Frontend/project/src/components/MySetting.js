import React from 'react';
import MaterialBox from './MaterialBox';
import ModifyPwdForm from './ModifyPwdForm';
import PersonalInfoForm from './PersonalInfoForm';

class MySetting extends React.Component
{
    render()
    {
        return (
            <div className='container border mt-4'>
                <ul className='nav nav-tabs nav-fill mt-4' role="tablist">
                    <li className='nav-item' role="presentation">
                        <button className="nav-link active" data-bs-toggle="tab" data-bs-target="#personalInfo">個人名片</button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#personalInfoEdit">編輯個人資訊</button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#modifyPwd">修改密碼</button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button className="nav-link" data-bs-toggle="tab" data-bs-target="#materialBox">教學素材箱</button>
                    </li>
                </ul>
                <div className='tab-content mb-3'>
                    <div className="tab-pane fade show active" id="personalInfo">
                        個人檔案
                    </div>
                    <div className="tab-pane fade" id="personalInfoEdit">
                        <PersonalInfoForm />
                    </div>
                    <div className="tab-pane fade" id="modifyPwd">
                        <ModifyPwdForm/>
                    </div>
                    <div className="tab-pane fade" id="materialBox">
                        <MaterialBox/>
                    </div>
                </div>
            </div>
        );
    }
}

export default MySetting;