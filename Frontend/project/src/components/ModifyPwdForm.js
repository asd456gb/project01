import React from 'react';

class ModifyPwdForm extends React.Component
{
    handleSumit=(e)=>{
        e.preventDefault();
        console.log(e);
        // TODO: modify api interface and util function
    }
    render()
    {
        return (
            <form className='border p-4' onSubmit={this.handleSumit}>
                <div className="input-group mb-3">
                    <span className='input-group-text'>原先密碼</span>
                    <input type="text" className='form-control' placeholder='若不想修改密碼，請保持空白'/>
                </div>
                <div className="input-group mb-3">
                    <span className='input-group-text'>新密碼</span>
                    <input type="text" className='form-control' placeholder='若不想修改密碼，請保持空白'/>
                    <span className='input-group-text'>確認密碼</span>
                    <input type="text" className='form-control' placeholder='若要修改密碼，請再輸入一次，以免忘記'/>
                </div>
                <div className='d-grid gap-2'>
                    <button className='btn btn-primary'>確認</button>
                </div>
            </form>
        );
    }
}

export default ModifyPwdForm;