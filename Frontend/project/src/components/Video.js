import React from 'react';

class Video extends React.Component
{
    constructor(props)
    {
        super(props);
        this.video=React.createRef();
    }
    waitRef=(callback)=>{
        if(!this.video.current)
        {
            setTimeout(()=>{
                this.waitRef(callback);
            },500);
        }
        else
            callback();
    }
    componentDidMount()
    {
        this.waitRef(()=>{
            this.video.current.srcObject=this.props.srcObject;
        })
    }
    render()
    {
        return <video className={this.props.className} ref={this.video} autoPlay playsInline></video>
    }
}

export default Video;
