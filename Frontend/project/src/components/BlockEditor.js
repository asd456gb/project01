import Editor from '@monaco-editor/react';
import React from 'react';
import util from '../util';
import BlocklyComponent from './BlocklyComponent';

class BlockEditor extends React.Component
{
    constructor(props)
    {
        super(props);
        this.state={
            blockDefId: `blockDef-${util.newId()}`,
            blockDefEditor: null,

            blockCodeDefId: `blockCodeDef-${util.newId()}`,
            blockCodeDefEditor: null,

            blockly: null, //blocks and addBlock
            updateTimes: 0,
        };
    }
    setUpdateTimes=()=>{
        this.setState({updateTimes: this.state.updateTimes+1});
    }
    handleBlockDefEditorDidMount=(editor,monaco)=>this.setState({blockDefEditor: editor});
    handleBlockCodeDefEditorDidMount=(editor,monaco)=>this.setState({blockCodeDefEditor: editor});
    handleBlocklyDidMount=(blockly)=>
    {
        this.setState({blockly: blockly},()=>{
            if(this.props.onMount)
                this.props.onMount({getBlocks: this.state.blockly.getBlocks});
        });
    }
    addBlock=()=>{
        let block=JSON.parse(this.state.blockDefEditor.getValue());
        let code=this.state.blockCodeDefEditor.getValue();
        this.state.blockly.addBlock(block,code);
    }
    editBlock=(type)=>{
        let block=this.state.blockly.state.blocks[type];
        this.state.blockDefEditor.setValue(JSON.stringify(block.style,null,4));
        this.state.blockCodeDefEditor.setValue(block.code);
    }
    deleteBlock=(type)=>{
        this.state.blockly.deleteBlock(type);
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(this.props.id!==prevProps.id)
        {
            this.state.blockDefEditor.setValue(JSON.stringify(util.example.blockDef,null,4));
            this.state.blockCodeDefEditor.setValue(util.example.blockCodeDef);
        }
    }
    render()
    {
        let blockList;
        if(this.state.blockly&&this.state.blockly.state.blocks)
        {
            let blocks=this.state.blockly.state.blocks;
            blockList=<div className='d-grid gap-2'>
                {
                    Object.keys(blocks).map((key)=>{
                        return (
                            <React.Fragment key={key}>
                                <button className='btn btn-light dropdown-toggle' type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    {key}
                                </button>
                                <ul className='dropdown-menu'>
                                    <li className='dropdown-item' onClick={()=>{this.editBlock(key)}}>編輯</li>
                                    <li className='dropdown-item' onClick={()=>{this.deleteBlock(key)}}>刪除</li>
                                </ul>
                            </React.Fragment>
                        );
                    })
                }
            </div>
        }
        return (
            <React.Fragment>
                <ul className='nav nav-tabs nav-fill mt-4' role="tablist">
                    <li className='nav-item' role="presentation">
                        <button type='button' className="nav-link" data-bs-toggle="tab">
                            <a href='https://blockly-demo.appspot.com/static/demos/blockfactory/index.html' target="_blank" rel='noreferrer'>
                                積木產生工具
                            </a>
                        </button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button type='button' className="nav-link active" data-bs-toggle="tab" data-bs-target={`#${this.state.blockDefId}`}>積木樣式定義</button>
                    </li>
                    <li className='nav-item' role="presentation">
                        <button type="button" className="nav-link" data-bs-toggle="tab" data-bs-target={`#${this.state.blockCodeDefId}`}>積木產生程式碼定義</button>
                    </li>
                </ul>
                <div className='tab-content border'>
                    <div className="tab-pane fade show active" id={this.state.blockDefId}>
                        <Editor height="20vh" defaultLanguage='json' defaultValue={JSON.stringify(util.example.blockDef,null,4)} onMount={this.handleBlockDefEditorDidMount}/>
                    </div>
                    <div className="tab-pane fade" id={this.state.blockCodeDefId}>
                        <Editor height="20vh" defaultLanguage='javascript' defaultValue={util.example.blockCodeDef} onMount={this.handleBlockCodeDefEditorDidMount}/>
                    </div>
                </div>
                <div className='d-grid'>
                    <button type='button' className='btn btn-primary' onClick={this.addBlock}>新增/修改</button>
                </div>
                <div className='row'>
                    <div className='col-3'>
                        {blockList}
                    </div>
                    <div className='col-9'>
                        <BlocklyComponent className={this.props.className} style={this.state.style} blocks={this.props.blocks} id={this.props.id} onMount={this.handleBlocklyDidMount} onSync={this.setUpdateTimes}/>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

export default BlockEditor;