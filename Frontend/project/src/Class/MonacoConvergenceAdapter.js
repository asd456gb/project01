import { EditorContentManager, RemoteCursorManager, RemoteSelectionManager } from "@convergencelabs/monaco-collab-ext";
import { ColorAssigner } from "@convergence/color-assigner";



class MonacoConvergenceAdapter
{
    constructor(monacoEditor,realtimeString)
    {
        this.monacoEditor=monacoEditor;
        this.model=realtimeString;
        this.colorAssigner=new ColorAssigner();
    }

    bind()
    {
        this.initSharedData();
        this.initSharedCursors();
        this.initSharedSelection();
    }

    initSharedData=()=>
    {
        this.contentManager=new EditorContentManager({
            editor: this.monacoEditor,
            onInsert: (index,text)=>{
                this.model.insert(index,text);
            },
            onReplace: (index,length,text)=>{
                this.model.model().startBatch();
                this.model.remove(index,length);
                this.model.insert(index,text);
                this.model.model().completeBatch();
            },
            onDelete: (index,length)=>{
                this.model.remove(index,length);
            },
            remoteSourceId: "convergence"
        })
        this.model.events().subscribe(e=>{
            switch(e.name)
            {
                case 'insert':
                    this.contentManager.insert(e.index,e.value);
                    break;
                case 'remove':
                    this.contentManager.delete(e.index,e.value.length);
                    break;
                default:
                    break;
            }
        })
    }

    setLocalCursor=()=>{
        const position=this.monacoEditor.getPosition();
        const offset=this.monacoEditor.getModel().getOffsetAt(position);
        this.cursorReference.set(offset);
    }

    addRemoteCursor=(reference)=>{
        const color=this.colorAssigner.getColorAsHex(reference.sessionId());
        const remoteCursor=this.remoteCursorManager.addCursor(reference.sessionId(),color,reference.user().displayName);

        reference.on('cleared',()=>remoteCursor.hide());
        reference.on('disposed',()=>remoteCursor.dispose());
        reference.on('set',()=>{
            const cursorIndex=reference.value();
            remoteCursor.setOffset(cursorIndex);
        });
    }

    initSharedCursors=()=>{
        this.remoteCursorManager=new RemoteCursorManager({
            editor: this.monacoEditor,
            tooltips: true,
            tooltipDuration: 2
        });
        this.cursorReference=this.model.indexReference("cursor");

        const references=this.model.references({key: "cursor"});
        references.forEach((reference)=>{
            if(!reference.isLocal())
                this.addRemoteCursor(reference);
        });

        this.setLocalCursor();
        this.cursorReference.share();

        this.monacoEditor.onDidChangeCursorPosition((e)=>{
            this.setLocalCursor();
        });

        this.model.on("reference",(e)=>{
            if(e.reference.key()==="cursor")
                this.addRemoteCursor(e.reference);
        })
    }

    setLocalSelection=()=>{
        const selection=this.monacoEditor.getSelection();
        if(!selection.isEmpty())
        {
            const start=this.monacoEditor.getModel().getOffsetAt(selection.getStartPosition());
            const end=this.monacoEditor.getModel().getOffsetAt(selection.getEndPosition());
            this.selectionReference.set({start,end});
        }
        else if(this.selectionReference.isSet())
            this.selectionReference.clear();
    }

    addRemoteSelection=(reference)=>{
        const color=this.colorAssigner.getColorAsHex(reference.sessionId());
        const remoteSelection=this.remoteSelectionManager.addSelection(reference.sessionId(),color);

        if(reference.isSet())
        {
            const selection=reference.value();
            remoteSelection.setOffsets(selection.start,selection.end);
        }

        reference.on('cleared',()=>remoteSelection.hide());
        reference.on('disposed',()=>remoteSelection.dispose());
        reference.on("set",()=>{
            const selection=reference.value();
            remoteSelection.setOffsets(selection.start,selection.end);
        })
    }

    initSharedSelection=()=>{
        this.remoteSelectionManager=new RemoteSelectionManager({editor: this.monacoEditor});

        this.selectionReference=this.model.rangeReference("selection");
        this.setLocalSelection();
        this.selectionReference.share();

        this.monacoEditor.onDidChangeCursorSelection(e=>{
            this.setLocalSelection();
        })

        const references=this.model.references({key: "selection"});
        references.forEach((reference)=>{
            if(!reference.isLocal())
                this.addRemoteSelection(reference);
        })

        this.model.on("reference",(e)=>{
            if(e.reference.key()==="selection")
                this.addRemoteSelection(e.reference);
        })
    }
}

export default MonacoConvergenceAdapter;